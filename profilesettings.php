<?php
    include_once "header/header_index.php";
    include "classes/dbh.classes.php";
    include "classes/models/profileinfo-model.classes.php";
    include "classes/views/profileinfo-view.classes.php";
    $profileInfo = new ProfileInfoView();
?>
<head>
    <title>Actualizar Perfil | Guía de Suplementos Deportivos</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/styles_profilesettings.css">
</head>

    <section class="formulario seccion-gris">
        <div class="container">
            <div class="row">
                <div class=" col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                    <form action="includes/profileinfo.inc.php" method="POST" id="formulario" autocomplete="off">
                        <h2 class="font-weight-bold mb-3">Actualizar perfil</h2>
                        <div class="form-group">
                            <label class="label" for="nombres"><strong>Nombres:</strong></label>
                            <input type="text" class="form-control" id="nombres" name="nombres"
                                value="<?php $profileInfo->fetchName($_SESSION["correo_session"]); ?>" required>
                        </div>
                        <div class="form-group">
                            <label class="label" for="apellidos"><strong>Apellidos:</strong></label>
                            <input type="text" class="form-control" id="apellidos" name="apellidos"
                                value="<?php $profileInfo->fetchLastname($_SESSION["correo_session"]); ?>" required>
                        </div>

                        <div class="form-group">
                            <label class="label" for="telefono"><strong>Teléfono:</strong></label>
                            <input type="text" class="form-control" id="telefono" name="telefono"
                                value="<?php $profileInfo->fetchPhone($_SESSION["correo_session"]); ?>" pattern="[0-9]{10}" required>
                        </div>

                        <div class="form-group">
                            <label class="label" for="correo"><strong>Correo:</strong></label>
                            <input readonly type="email" class="form-control" id="correo" name="correo"
                                value="<?php echo ($_SESSION["correo_session"]); ?>" required>
                        </div>
                        <div class="botones">
                            <div class="boton">
                                <form action="" method="POST" id="formulario" autocomplete="off">
                                    <input class="btn_guardar" type="submit" value="Guardar" id="btn_guardar"
                                        name="btn_guardar">
                                </form>
                            </div>
                            <div class="boton">
                                <button type="button" id="btn_cancelar" class="btn_cancelar"
                                    onclick="window.location.href = 'profile.php'">Cancelar</button>
                            </div>
                        </div>
                    </form>
                    <?php
                        if (!isset($_GET['error'])) {
                            
                        }
                        else {
                            $errorCheck = $_GET['error'];
                            
                            if ($errorCheck == "emptyinput") {
                                echo "<p class='error'>¡Ups! Parece que te has olvidado de rellenar algunos campos.</p>";
                            }
                            elseif ($errorCheck == "nameorlastname") {
                                echo "<p class='error'>¡Vaya! Parece que has utilizado algunos caracteres no válidos.</p>";
                            }
                            elseif ($errorCheck == "phone") {
                                echo "<p class='error'>¡Oh no! Parece que has ingresado un número de teléfono no válido.</p>";
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>

    <!-- Sección pie de pagina-->
    <footer>
        <div class="useful-links">
            <ul>
                <img src="img/logo_blanco.png" class="mb-3" width="100" alt="Logo de la pagina">
                <li><a href="index_log.php">Inicio</a></li>
                <li><a href="https://sanmonor.github.io/Proyecto_Suplementos_Deportivos/" target="_blank" rel="noopener noreferrer">Nuestro Portafolio</a></li>
                <li><a href="#">Términos y condiciones</a></li>
            </ul>
        </div>
        <div class="copyright">
            <p>&copy; 2023 Guía de suplementos deportivos. Todos los derechos reservados.</p>
        </div>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>
</body>

</html>