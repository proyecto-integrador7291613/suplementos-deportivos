<?php
    include_once "header/header_index.php";
    include "includes/catalogo.inc.php";
    $nProductos = $vista->fetchProductsNumber();
    
?>

<head>
    <title><?php echo $palabraClave;?> | Guía de Suplementos Deportivos</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/styles_catalogo.css">
</head>

<!-- Lista de Productos -->
<section class="catalogo" style="padding-top: 104px; padding-bottom: 50px;">
    <div class="container">
        <div class="title text-center py-4">
            <h2 class="position-relative d-inline-block">
                <?php
                echo $palabraClave;
                ?>
            </h2>
        </div>
        <?php
            if ($nProductos > 1) {
        ?>
            <span class="badge bg-secondary">
                <?php echo $nProductos; ?> resultados
            </span>
        <?php
            } else {
        ?>
            <span class="badge bg-secondary">
                <?php echo $nProductos; ?> resultado
            </span>
        <?php
            }
        ?>
        <div class="row g-0">
            <?php
                for ($i=0; $i < $nProductos; $i++) { 
            ?>
            <div class="col-md-6 col-lg-4 col-xl-3 p-2">
                <a href="producto.php?producto=<?= urlencode($vista->fetchProductId($i)) ?>">
                    <div class="card h-100 d-flex">
                        <div
                            class="card-img position-relative overflow-hidden h-100 d-flex flex-column justify-content-center align-items-center">
                            <img src="<?= $vista->fetchProductFoto($i) ?>" alt="Imagen del suplemento">
                        </div>
                        <div class="card-body h-30 border align-items-end">
                            <h3>
                                <?= $vista->fetchProductName($i) ?>
                            </h3>
                            <h5>
                                <?= $vista->fetchProductCategory($i) ?>
                                <span>
                                    <i class="<?= $vista->verificarFavorito($i) ? 'fa-solid' : 'fa-regular' ?> fa-heart guardar-favorito" data-producto="<?php echo $vista->fetchProductId($i); ?>"></i>
                                    <!-- operadores ternarios: condición ? valor_si_verdadero : valor_si_falso-->
                                </span>
                            </h5>
                            
                        </div>
                    </div>
                </a>
            </div>
            <?php 
                } ?>  
            <script>
                document.addEventListener('DOMContentLoaded', function () {
                // Espera a que el DOM esté completamente cargado antes de ejecutar el código
                var corazones = document.querySelectorAll('.guardar-favorito');
                // Busca todos los elementos que tienen la clase 'guardar-favorito'
                corazones.forEach(function (corazon) {
                    // Itera sobre cada uno de esos elementos
                    corazon.addEventListener('click', function (event) {
                        // Agrega un evento de clic a cada elemento
                        event.preventDefault(); // Evita el comportamiento predeterminado del enlace
                        var idProducto = this.getAttribute('data-producto');
                        // Obtiene el valor del atributo 'data-producto' de cada elemento, que contiene el ID del producto
                        window.location.href = 'includes/guardar-fav.inc.php?producto=' + idProducto + '&palabraClave=' + encodeURIComponent('<?php echo $palabraClave; ?>') + '&from=catalogo';
                        // Redirige al usuario a 'guardar_favorito.php' con el ID del producto como parámetro en la URL
                    });
                });
            });
            </script> 
        </div>
    </div>
</section>

    <!-- Sección pie de pagina-->
    <footer>
        <div class="useful-links">
            <ul>
                <img src="img/logo_blanco.png" class="mb-3" width="100" alt="Logo de la pagina">
                <li><a href="index_log.php">Inicio</a></li>
                <li><a href="https://sanmonor.github.io/Proyecto_Suplementos_Deportivos/" target="_blank"
                        rel="noopener noreferrer">Nuestro Portafolio</a></li>
                <li><a href="#">Términos y condiciones</a></li>
            </ul>
        </div>
        <div class="copyright">
            <p>&copy; 2023 Guía de suplementos deportivos. Todos los derechos reservados.</p>
        </div>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>
</body>

</html>