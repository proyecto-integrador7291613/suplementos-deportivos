drop database if exists guia_suplementos;
create database guia_suplementos;
use guia_suplementos;

create table Categorias(
	id int primary key auto_increment, 
	nombre varchar(35) not null
);

create table Marcas(
	id int primary key auto_increment,
	nombre varchar(35) not null
);

create table Usuarios(
	id int primary key auto_increment,
	nombres varchar(45) not null,
	apellidos varchar(45) not null,
	telefono varchar(12) not null unique,
	email varchar(100) not null unique,
	contrasena varchar(255) not null
);

create table Suplementos(
	id int primary key auto_increment,
	nombre varchar(45) not null,
    foto varchar(255) not null,
	peso_lb float,
	peso_g float,
    n_tabletas int,
	categoria_id int not null,
	marca_id int not null,
	descripcion text not null,
	tamano_x_porcion float not null,
    medida_x_porcion float,
    tipo_medida varchar(25),
	porciones_x_envase int not null,
	ingredientes text not null,
	modo_empleo text not null,
	foreign key (categoria_id) references Categorias(id),
	foreign key (marca_id) references Marcas(id)
);

create table Nutrientes(
	id int primary key auto_increment,
    nombre varchar(80) not null
);

create table Nutrientes_x_suplemento(
	id int primary key auto_increment,
    suplemento_id int not null,
    nutriente_id int not null,
    nutriente_valor float,
    nutriente_unidades varchar(10),
    porcentaje_valor_diario float,
    foreign key (suplemento_id) references Suplementos(id),
    foreign key (nutriente_id) references Nutrientes(id)
);

create table Historial(
	id int primary key auto_increment,
	fecha_hora date not null,
	usuario_id int not null,
	suplemento_id int not null,
	foreign key (usuario_id) references Usuarios(id),
	foreign key (suplemento_id) references Suplementos(id)
);

create table Favoritos (
	id int primary key auto_increment,
    usuario_id int not null,
    suplemento_id int not null,
	foreign key (usuario_id) references Usuarios(id),
	foreign key (suplemento_id) references Suplementos(id)
);

-- Categorías
insert into Categorias(nombre) values ('Proteínas');
insert into Categorias(nombre) values ('Aminoácidos');
insert into Categorias(nombre) values ('Creatinas');
insert into Categorias(nombre) values ('Quemadores');
insert into Categorias(nombre) values ('Pre-entrenos');
insert into Categorias(nombre) values ('Multivitamínicos');

-- Marcas
insert into Marcas(nombre) values ('ProScience');
insert into Marcas(nombre) values ('Nutramerican');
insert into Marcas(nombre) values ('Muscletech');
insert into Marcas(nombre) values ('Smart Muscle');

-- Nutrientes
insert into Nutrientes(nombre) values ('Calorías');
insert into Nutrientes(nombre) values ('Calorías grasa');
insert into Nutrientes(nombre) values ('Grasa Total');
insert into Nutrientes(nombre) values ('Grasa Saturada');
insert into Nutrientes(nombre) values ('Grasa Trans');
insert into Nutrientes(nombre) values ('Colesterol');
insert into Nutrientes(nombre) values ('Sodio');
insert into Nutrientes(nombre) values ('Carbohidratos Totales');
insert into Nutrientes(nombre) values ('Azúcares');
insert into Nutrientes(nombre) values ('Proteína');
insert into Nutrientes(nombre) values ('Vitamina A');
insert into Nutrientes(nombre) values ('Hierro');
insert into Nutrientes(nombre) values ('Calcio');
insert into Nutrientes(nombre) values ('Vitamina B6');
insert into Nutrientes(nombre) values ('Vitamina B1');
insert into Nutrientes(nombre) values ('Vitamina D');
insert into Nutrientes(nombre) values ('Vitamina C');
insert into Nutrientes(nombre) values ('L-Leucina');
insert into Nutrientes(nombre) values ('Fibra Dietaria');
insert into Nutrientes(nombre) values ('L-Glutamina');
insert into Nutrientes(nombre) values ('L-Isoleucina');
insert into Nutrientes(nombre) values ('L-Valina');
insert into Nutrientes(nombre) values ('Potasio');
insert into Nutrientes(nombre) values ('Niacina');
insert into Nutrientes(nombre) values ('Hidroximetilbutirato (HMB)');
insert into Nutrientes(nombre) values ('Ácido Alfalipoico');
insert into Nutrientes(nombre) values ('Sulfato de Vanadio');
insert into Nutrientes(nombre) values ('Creatina monohidrato');
insert into Nutrientes(nombre) values ('Vitamina E');
insert into Nutrientes(nombre) values ('Tiamina');
insert into Nutrientes(nombre) values ('Riboflavina');
insert into Nutrientes(nombre) values ('Folato');
insert into Nutrientes(nombre) values ('Vitamina B12');
insert into Nutrientes(nombre) values ('Biotina');
insert into Nutrientes(nombre) values ('Ácido Pantoténico');
insert into Nutrientes(nombre) values ('Colina');
insert into Nutrientes(nombre) values ('Magnesio');
insert into Nutrientes(nombre) values ('Zinc');
insert into Nutrientes(nombre) values ('Cobre');
insert into Nutrientes(nombre) values ('Manganeso');
insert into Nutrientes(nombre) values ('Molibdeno');
insert into Nutrientes(nombre) values ('Glicina');
insert into Nutrientes(nombre) values ('L-Metionina');
insert into Nutrientes(nombre) values ('L-Citrulina Malato');
insert into Nutrientes(nombre) values ('L-Arginina HCI');
insert into Nutrientes(nombre) values ('L-Cisteína');
insert into Nutrientes(nombre) values ('Extracto de Hoja de Té Verde');
insert into Nutrientes(nombre) values ('Extracto de la Fruta de la Palma Enana Americana');
insert into Nutrientes(nombre) values ('Hierba Entera de Ginseng Americano');
insert into Nutrientes(nombre) values ('Raíz de Equinácea');
insert into Nutrientes(nombre) values ('Hoja de Ginkgo');
insert into Nutrientes(nombre) values ('Extracto de Piel de Uva');
insert into Nutrientes(nombre) values ('Extracto de Grano de Café Verde');
insert into Nutrientes(nombre) values ('Extracto de Rizoma de Cúrcuma');
insert into Nutrientes(nombre) values ('Octacosanol');
insert into Nutrientes(nombre) values ('Cloruro de Potasio');
insert into Nutrientes(nombre) values ('Bitartrato de Colina');
insert into Nutrientes(nombre) values ('Citrato de Colina Dihidrogenado');
insert into Nutrientes(nombre) values ('Inositol');
insert into Nutrientes(nombre) values ('Boro');
insert into Nutrientes(nombre) values ('Amilasa');
insert into Nutrientes(nombre) values ('Papaína');
insert into Nutrientes(nombre) values ('L-Lisina');
insert into Nutrientes(nombre) values ('L-Treonina');
insert into Nutrientes(nombre) values ('L-Histidina');
insert into Nutrientes(nombre) values ('L-Fenila');
insert into Nutrientes(nombre) values ('L-Triptófan');
insert into Nutrientes(nombre) values ('Cromo');
insert into Nutrientes(nombre) values ('L-Cartinitina');
insert into Nutrientes(nombre) values ('Picolinato de Cromo');
insert into Nutrientes(nombre) values ('Glutamina');
insert into Nutrientes(nombre) values ('Creatina');
insert into Nutrientes(nombre) values ('Leucina');
insert into Nutrientes(nombre) values ('Grasa poli-insaturada');
insert into Nutrientes(nombre) values ('Grasa mono-insaturada');
insert into Nutrientes(nombre) values ('Ácido linoleico conjugado');
insert into Nutrientes(nombre) values ('L-carnitina l-tartrato');
insert into Nutrientes(nombre) values ('Extracto de garcinia indica');
insert into Nutrientes(nombre) values ('Extracto de C. canephora robusta');
insert into Nutrientes(nombre) values ('Cetona de frambuesa');
insert into Nutrientes(nombre) values ('Betaína anhidra');
insert into Nutrientes(nombre) values ('Beta-alanina');
insert into Nutrientes(nombre) values ('Taurina');
insert into Nutrientes(nombre) values ('Nitrosigina Silicato de arginina-inositol');
insert into Nutrientes(nombre) values ('Extracto de espino (como Crataegus pinnatifida) (baya)');
insert into Nutrientes(nombre) values ('Cafeína ahnidra');
insert into Nutrientes(nombre) values ('L-teanina');
insert into Nutrientes(nombre) values ('Extracto de Galanga (como Alpinia officinarum) (tallo y raíz)');
insert into Nutrientes(nombre) values ('Extracto de Yohimbe (como Pausinystalia yohimbine) (corteza)');
insert into Nutrientes(nombre) values ('Calorías de la grasa');
insert into Nutrientes(nombre) values ('Carbohidrato Total');
insert into Nutrientes(nombre) values ('Vitamina B3');
insert into Nutrientes(nombre) values ('L-Cilrulina');
insert into Nutrientes(nombre) values ('L-Alanina');
insert into Nutrientes(nombre) values ('L-Arginina');
insert into Nutrientes(nombre) values ('Creatina HCL');
insert into Nutrientes(nombre) values ('Polvo de Raiz de Remolacha');
insert into Nutrientes(nombre) values ('Betaina');
insert into Nutrientes(nombre) values ('Selenito de Sodio');
insert into Nutrientes(nombre) values ('Pimienta en Polvo');
insert into Nutrientes(nombre) values ('Piridoxina');
insert into Nutrientes(nombre) values ('Ácido fólico');
insert into Nutrientes(nombre) values ('Vitamina K');
insert into Nutrientes(nombre) values ('Cloro');
insert into Nutrientes(nombre) values ('Selenio');
insert into Nutrientes(nombre) values ('Yodo');
insert into Nutrientes(nombre) values ('Coenzima Q10');
insert into Nutrientes(nombre) values ('Ácido R-Alfa Lipoico');
insert into Nutrientes(nombre) values ('Bioperina');
insert into Nutrientes(nombre) values ('Azucares añadidos');
insert into Nutrientes(nombre) values ('Fibra soluble');
insert into Nutrientes(nombre) values ('Fibra insoluble');
insert into Nutrientes(nombre) values ('Azucares');
insert into Nutrientes(nombre) values ('Azucares Totales');
insert into Nutrientes(nombre) values ('Acido Alfa Linolénico (Omega 3)');
insert into Nutrientes(nombre) values ('Vitamina D3');
insert into Nutrientes(nombre) values ('Acido Folico');
insert into Nutrientes(nombre) values ('Vitamina K2');
insert into Nutrientes(nombre) values ('Acido pantotenico');
insert into Nutrientes(nombre) values ('Lodine');
insert into Nutrientes(nombre) values ('Betaína anhidra');
insert into Nutrientes(nombre) values ('Citrato de Sodio');
insert into Nutrientes(nombre) values ('Agua de Coco');
insert into Nutrientes(nombre) values ('Jugo de Sandia');
insert into Nutrientes(nombre) values ('Cloro de Sodio');
insert into Nutrientes(nombre) values ('Fosfato de dipotásio');
insert into Nutrientes(nombre) values ('Ácido alfa lipoico');
insert into Nutrientes(nombre) values ('Clorhidrato de Creatina');
insert into Nutrientes(nombre) values ('Creatina de ácido libre');
insert into Nutrientes(nombre) values ('l-Tyrosina');
insert into Nutrientes(nombre) values ('L-Taurina');






-- Suplementos
	-- 1
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'BEST PROTEIN', './img/best.png', 2.18, 990, 1, 1, 
'Proteína aislada de suero de leche de alta calidad 65%, caseína Micelar 35% y Leucina 3g. Este producto no contiene azúcar, lactosa, grasa. Sabor Vainilla Gourmet',
33, 1, 'Cuchara', 30, 
'Proteína Aislada de Suero, Caseína Micelar, L-Leucina, Goma Guar, Vitamina C (ÁcidoAscórbico), Vainilla Gourmet (Saborizante Idéntico al Natural), Stevia (Edulcorante), Dióxido de Silicio, Vitamina B6 (Piridoxina), Vitamina B1 (Tiamina), Vitamina D3', 
'Mezcla una cucharada de Best Protein con 5-6 onzas de agua (o leche para aumentar la cremosidad y las calorías). El momento ideal para tomarla es después del entrenamiento. También puedes tomar la proteína como bocadillo entre las comidas. Si tu objetivo es perder grasa, debes consumir solo una porción por día, si tu objetivo es mantener o aumentar la masa muscular, toma dos porciones por día');

	-- 2
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'NITRO SHOCK', './img/nitro.png', 1.15, 520, 2, 2,
'Aminoácidos de Cadena Ramificada y Glutamina. Ratio 10:5:5  Alimento en polvo a base de Maltodextrina. Sabor Frutos rojos',
20, 4, 'medida', 26,
'Maltodextrina, mezcla de aminoácidos de cadena ramificada (l-leucina, l-valina, l-isoleucina), l-glutamina, ácido cítrico (acidulante), sabor artificial idéntico al natural (frutos rojos), citrato de sodio (regulador de acidez), bicarbonato de sodio (regulador de acidez), fosfato monopotásico (regulador de acidez), sacarina sódica (edulcorante artificial DMU 1200 mg/kg), colorante artificial (azorrubina, azul brillante fcf)',
'Mezcle 20g de NITRO SHOCK (4 cucharadas) en 400ml de agua; consuma la mitad durante la actividad y la otra mitad al finaliar el entrenamiento. Incluye medidor (cuchara dosificadora)');

	-- 3
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'CREA STACK', './img/crea.png', 1.3, 600, 3, 2,
'CREA STACK es la última generación en suplementos con Creatina, su fórmula única en el mercado incluye HMB, así como ácido Alfa Lipoico y Sulfato de Vanadio, es ideal para que alcances los objetivos con mayor facilidad.',
'10', '2', 'Cuchara', 60,
'Maltodextrina, Monohidrato de Creatina, Ácido Cítrico (acidulante), Hidroximetilbutirato de Calcio(HMB), Sabor Artificial idéntico al natural (Frutos Rojos), Citrato de Sodio (regulador de acidez), Sacarina Sódica (edulcorante artificial), Fosfato monopotasico (regulador de acidez), Ácido Alfa lipoico, Vanadil Sulfato, Niacina, Dióxido de Silicio Coloidal (anticompactante), Colorante artificial (Rojo No.5)',
'Mezcle 2 medidas de Crea Stack, con 200ml de agua o adicione su bebida favorita. Consuma uno o más servicios diario. Se puede tomar antes del entreno, después del entreno con la proteína o al despertar');
    
	-- 4
insert into Suplementos(nombre, foto, n_tabletas, categoria_id, marca_id, descripcion, tamano_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'PLATINUM MULTIVITAMIN', './img/platinum.png', 90, 6, 3,
'Platinum Multivitamin es un multivitamínico diario formulado para apoyar las necesidades nutricionales de un estilo de vida saludable y activo. Los entrenadores intensos pueden perder vitaminas y minerales críticos debido al entrenamiento. Platinum Multivitamin proporciona los micronutrientes más críticos para apoyar la salud general y el rendimiento atlético.',
3, 'Tableta',  30,
'Celulosa microcristalina, croscarmelosa sódica, aceite vegetal hidrogenado, revestimiento (talco, polietilenglicol, polisorbato 80), estearato de magnesio, dióxido de silicio',
'Tomar 1 dosis (3 comprimidos) al día con un vaso de agua preferiblemente con una comida principal');

	-- 5
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'ARMY', './img/army.png', '1.2','540', 2, 1, 
'Con aminoácidos, y EAA(Leucina, Isoleucina, Valina, Lisina, Treonina, Histidina, Fenilalanina, Metionina, Triptófano)', 
18, 1, 'Cuchara', 30, 
'Polvo Deshidratado de Frutos Amarillos, L-Glutamina, L-Leucina, L-Isoleucina, L-Valina, L-Lisina, L-Treonina, L-Histidina, L-Fenilalanina, L-Metionina, L-Triptófano, Carboximetilcelulosa (CMC),Fosfato Tricálcico, Saborizante idéntico al Natural, Vitamina C (Ácido Ascórbico), Stevia,Citrato de Sodio, Citrato de Potasio, Ácido Málico, Dióxido de Silicio, Colorante (Amarillo), Citrato de Magnesio, Gluconato de Zinc.',
'Toma 1 scoop (medidor incluido) en 1000ml de agua, tomar durante la actividad física.'); 
     
    -- 6
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'BURNER STACK', './img/burner.png', '0.79', '360', 4, 2, 
'Es un producto de última generación diseñado para ayudar a cumplir tus objetivos facilmente, contiene los ingredientes más estudiados y con eficacia comprobada. Contiene 6g por porción de ingredientes, esta así diseñado para tener el 100% de las cantidades recomendadas, que a diferencia de los productos en cápsulas o tabletas solo contienen unas pequeñas dosis de estos.',
6, 1, 'Cuchara', 60, 
'Extracto de naranja amarga, CLA, L-Carnitina, Cafeína, Goma guar, Picolinato de cromo y taurina.',
'Mezcle Burner Stack 6 g (1 medida/cucharada) en 315 ml de agua. Tome una porción al despertar y otra antes de almorzar (preferiblemente 1/2 hora antes). Incluye medidor (cuchara dosificadora)');
      
-- 7
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'BI-PRO RIPPED', './img/Bi_Pro_Ripped.png', '2.4', '1080', 1, 2,
'Bipro ripped contiene los ingredientes más estudiados y evidenciados por la ciencia para el incremento de la 
masa muscular y la perdida de grasa: Proteína de suero de leche 30g, 1000mg l-carnitina y 400mcg de picolinato 
de chromo. Contiene una pequeña cantidad de carbohidratos para reponer la glucosa perdida durante el entreno 
y así evitar que la proteína se transforme en glucosa. ',
54, 1, 'Cuchara', 20, 
'Proteína de Suero de leche bovina (Beta-Lactoglobulina, Alfa-Lactoalbúmina, albúmina de suero Bovino, Inmunoglobulinas, Lactoferrina), Maltodextrina, Proteína concentrada de lecho bovina (80% Caseína, 20% Proteína de suero), Avena, Fibra dietaria, L-Carnitina, sabor artificial idéntico al natural (Vainilla), Acesulfame de potasio (Edulcorante, DMU 2000mg/kg), Cloruro de sodio, Goma Xantana (espesante), Carboximetil celulosa (espesante), Bicarbonato de sodio (Regulador de acidez), Picolinato de cromo',
'Mezcle Bi-Pro Ripped 54g (1 medida/cucharada) en 300ml de agua o leche descremada. Consúmalo inmediatamente después de entrenar y antes de dormir. Si su entrenamiento es nocturno, úselo después de entrenar y con el desayuno. Para aumento de peso no reemplace ninguna comida con este producto. Incluye medidor (cuchara dosificadora)');
    
    -- 8
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'ATOMIC', './img/ATOMIC.png', '1.32', '600', 3, 4,
'Su formula contiene 5g de Creatina HCL, 3g de Leucina y 3g de Glutamina. Alimento en polvo de fruta micropulverizada. Sabor a frambuesa azul',
 2, 2, 'Cuchara', 30,
'Polvo de fruta micropulverizada, Creatina HCL, Glutamina, Leucina, Ácido Cítrico, CMC, Sucralosa, Dióxido de Silicio, Sabor Blueraspberry, Color artificial.',
'Para uso post entrenamiento: poner 2 scoops (1 serving) de Crea Tomic entre 220-250 ml de agua y mezclar.'
);

    -- 9
insert into Suplementos(nombre, foto, n_tabletas, categoria_id, marca_id, descripcion, tamano_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'HYDROXYCUT HARDCORE ELITE','./img/hydroxycut_hardcore_elite.png', 100, 4, 3,
'Contiene una potente mezcla de café verde, yohimbina, coleo, L-teanina, cafeína y teobromina. Esta fórmula súper termogénica acelera tu estilo de vida al proporcionar energía extrema',
 2, 'Cápsula', '50',
'Gelatina, glicerina, aceite de cártamo, cera de abeja blanca, lecitina de soja fraccionada, dióxido de silicio, sabor natural y artificial, agua purificada, dióxido de titanio, FD&C Red No. 3, FD&C Red No. 40, FD&C Blue No.1, FD&C Yellor No. 5. Contiene ingredientes de soja.',
'Las tolerancias individuales varían. Asegúrate de lograr tu tolerancia con cada etapa antes de aumentar su dosis. Día 1 y día 2: 1 cápsula al día. Día 3 y día 4: 2 cápsulas una vez al dia. Día 5 y día 6: 3 cápsulas, 2 antes de desayunar y 1 antes del almuerzo. Día 7 en adelante: 2 cápsulas 2 veces al día (2 cápsulas antes del desayuno y 2 cápsulas antes del almuerzo).');

   -- 10
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'VAPOR X5','./img/VAPOR_X5.png', 0.6, 272, 5, 3,
'Incluye monohidrato de creatina combinada con betaína. Alimento en polvo a base de carbohidratos. Sabor Blue Razz Freeze',
9, 1, 'Cuchara', 30,
'Ácido Cítrico, Dióxido de Silicio, sabores naturales y artificiales, Ácido Málico, Maltodextrina, Sucralosa, Acesulfame de Potasio, mezcla de gomas (Celulosa, Xantana, Carragenina, Dextrosa) y Colorante. Contiene ingredientes de la Leche y Soya',
'Agregar 1 servicio (medida dosificadora) a 4 onzas de agua fría y mezclar. No consumir en estado de embarazo y/o lactancia'); 

   -- 11 
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'INTENZE', './img/INTENZE.png', 1.9, 900, 5, 1,
'Contiene Cafeína, Creatina HCL, Citrulina, Beta-alanina y arginina. Alimento en polvo a base de fruta deshidratada con frutos rojos, polvo de remolacha. Sabor Fruit Punch',
30, 1, 'Cucharada', 30,
'Polvo Deshidratado De Frutos Rojos, L-Citrulina,Beta-Alanina, Creatina Hcl, Polvo De Raíz DeRemolacha, L-Leucina, L-Arginina, Betaina, L-Isoleucina, L-Valina, Dioxido De Silicio (Anticompactante), Cafeina, Sucralosa (Edulcorante), Saborizante Idéntico Al Natural, Goma Xanthan (Espesante), Citrato De Zinc, AcidoMalico (Acidulante), Niacina (Vitamina B3), Benzoato De Sodio (Conservante), Colorante Rojo, Pimienta En Polvo, Piridoxina Clorhidrato,TiaminaClorhidrato, Vitamina D3, Selenito De Sodio.',
'Mezclar un servicio de Intenze 30 g (1 scoop) en 625 ml de agua, tomar 20 a 30 minutos antes de iniciar la actividad física. Intenze no rompe el ayuno, lo pueden tomar adolecentes y adultos, se recomienda tomar antes de practicar alguna actividad deportiva.');

   -- 12
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'THE ONE', './img/THE_ONE.png', 0.66, 300, 6, 1,
'Alimento a base de polvo de fruta deshidratada con vitaminas y minerales sabor a Orange',
10, 1, 'Cuchara', 30,
'Polvo Deshidratado de Frutos Amarillos, Citrato de Calcio, Acidulante (Ácido Málico), Saborizante Idéntico al Natural (Orange), Coenzima Q10, Endulzante Artificial (Sucralosa), Anti compactante (Dióxido De Silicio), Lecitina de Soya (Estabilizante), Vitamina C, Ácido R-Alfa Lipoico, Cloruro de Potasio, Citrato de Magnesio, Niacina, Fumarato Ferroso, Citrato de Zinc, Ácido Pantoténico, Betacaroteno, Cloruro de Sodio, Bioperina, Citrato de Boro, Cobre, Piridoxina Clorhidrato Riboflavina, Vitamina A Retino Acetato, Tiamina, Vitamina D, Vitamina E, Biotina, Yoduro de Potasio, Selenito de Sodio, Vitamina K.',
'Mezclar un servicio de 10 gr (1 Scoop) en 250 ml de agua, agítese hasta tener un producto homogéneo, consúmase una vez preparado. Consume este producto a cualquier hora, preferiblemente en la mañana. Lo pueden consumir personas adultos mayores.');

-- 13
 insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
  'NITRO TECH WHEY GOLD','./img/Nitro_Tech_Whey_Gold.png', 2.0, 9, 1, 3,
  'Alimento en polvo a base de proteina de suero',
  33, 1, 'Cuchara', 31,
  'Mezcla Premium de proteina (péptidos de suero, aislado de proteina de suero de leche, aislado de proteina de suero de leche 97%), concetrado de proteina de suero , sabor natural y artificial, lecitina de soja, mezcla de gomas (celulosa, xantana, carragenina y dextrosa), sal dióxido de silicio, sucralosa, Acesulfame de potasio. Contiene leche e ingredientes de soja. Procesado en una planta que tambien fabrica productos con ingredientes de huevo, trigo y nueces.',
  'Agregar un servicio (medida dosificadora) a 6 onzas de agua fria o leche descremada y mezclar.');

 -- 14
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
  'PROTON WHEY','./img/PROTON_WHEY.png', 2.05, 907, 1, 4,
  'Mezcla a base de proteina aislada de suero lacteo, sabor artificial a summer vanilla antiespumante, edulcorante (sucralosa) y probioticos (Lactobacillus coagulans',
  31, 1, 'Cuchara', 30,
  '75% proteína concentrada y 25% proteína aislada, sabor summer vainilla o Chocolatte, antiespumante, sucralosa y probióticos (lactobacillus coagulans).',
  'Adicionar 1 Scoop (31 grs) de la mezcla en polvo a un vaso o shaker con 250 ml de agua. Como suplemento, entre comidas principales y/o después de actividad física.');

 -- 15
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
 'BI-PRO CLASSIC','./img/Bi_pro_classic.png', 2.0, 910, 1, 2,
 'Proteina de suero de leche aislada, contiene 42g de proteína en 1 scoop, 180 calorias, 35 servicios, libre de carbohidratos y grasas, contiene alfa lactoalbúmina y beta lactoglobulina. Proteína de alto valor biológico.',
  26, 1.2, 'Cuchara', 35,
  'Proteina de suero de leche bovina (alta tecnología, (contiene Beta-Lactoglobulina, Alfa-Lactoalbúmina y pequeñas cantidades de albúmina de suero Bovino, Inmunoglobulinas y Lactoferrina))',
  'Mezcle Bi pro classic 26g (1/2 medida/cucharada) con 4 onzas de agua o adicione su bebida favorita. Consúmalo inmediatamente después de entrenar. No reemplace ninguna comida con este producto. Los requerimientos proteicos varian de acuerdo a cada persona, basado en el peso corporal, nivel de actividad física y salud general.');
  
  -- 16
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
 'SMART','./img/Bi_pro_science.png', 3.25, 147, 1, 1,
 'Alimento en polvo a base de harina de avena con palatinosa, amilopectina, proteína de suero de leche enriquecido con vitaminas.',
  246, 6, 'Cuchara', 6,
  'Harina de Avena, Palatinosa, Amilopectina, Proteína de Suero de Leche, Caseinade Leche, Sabor Idéntico al Natural, Carboximetilcelulosa CMC (Estabilizante), Lecitina de Soya(Estabilizante), Goma Xanthan (Espesante), Edulcorante Natural (Stevia), Dióxido de Silicio(Anticompactante) (1000 mg/kg), Omega 3, Vitamina C (Ácido Ascórbico), Lactasa en polvo, Citrato de Zinc, Vitamina B6 (Piridoxina), Vitamina B1 (Tiamina), Vitamina D3.',
  'Mezclar 6 cucharas (246 g) del producto en 700 ml de agua, agitar hasta tener un producto homogéneo: Consumase una vez preparado.');
  
  -- 17
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
 'BEST VEGAN','./img/BEST VEGAN.png', 2.16, 180, 1, 1,
 'Alimento en polvo a base de proteína de arveja con BCAA, enriquecido con vitaminas y minerales sabor chocolate y vainilla. ',
  35, 1, 'Cuchara', 28,
 'Proteína de Arveja, Proteína de Arroz Orgánica, BCAA (L-Leucina, L-Isoleucina, L- Valina), Leche de Coco en polvo, Cocoa, Sabor idéntico al natural (Chocolate), Goma Xanthan (Espesante), edulcorante natural (Stevia), Vitamina C (Ácido Ascórbico), Citrato de Sodio, Fumarato ferroso, Vitamina E, Lactobacillus Paracasei (Cultivo probiótico), Vitamina B6(Piridoxina), Biotina, Vitamina B1 (Tiamina), Betacaroteno, Vitamina D3.',
  'Mezclar 1 cuchara (35g) del producto en 300ml de agua, agítese hasta obtener un producto homogéneo, consumase una vez preparado. ');
  
  -- 18
  insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
 'LEGACY','./img/LEGACY.png', 1.21, 550, 3, 1,
 'Alimento en polvo de frutas deshidratadas, polvo de fruta de uva, sabor uva. ',
  11, 1, 'Cuchara', 50,
 'Polvo Deshidratado de Frutas de Uva, Creatina HCL, Sucralosa (Edulcorante), Saborizante Idéntico al Natural, Aspartame (Edulcorante), Goma Xanthan (Espesante), Benzoato de Sodio (Conservante), Ácido Citrico (Acidulante), Ácido Málico (Acidulante), Dióxido de Silicio(Anticompactante), Silicato de Calcio (Espesante), Colorante Rojo FD&C CI 45430. Fenilcetonuricos: Contiene Fenilalanina.',
  'Mezclar 1 cuchara (11 g) del producto en 300 ml de agua, agítese hasta obtener un producto homogéneo, consumase una vez preparado.');
  
  -- 19
  insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
 'INTENZE NITE','./img/INTENZE NITE.png', 1.9, 900, 5, 1,
 'Intenze Nite Preworkout Proscience tiene todo lo que funciona en las dosis que funcionan sin producir insomnio. Aumenta tus niveles de energia, potencia, velocidad de recuperacion y fuerza. Contiene L Citrulina, Creatina HCL, BCAA, Beta Alanina y Arginina. Te invitamos entonces a consumir este delicioso cóctel de ayudas ergogénicas seguras, bien estudiadas y con mucha evidencia científica, para que tus entrenos sean mucho mejores.',
  30, 1, 'Cuchara', 30,
 'Cada servicio de Intenze Nite Preworkout está compuesto por 30 gramos de producto. 0 calorías (no rompe el ayuno) 6 gramos de L-Citrulina, 3 gramos de Beta Alanina, 3 gramos de Arginina, 3 gramos de Creatina HCL, 2.5 gramos de polvo de raíz de remolacha, 4 gramos de BCAA y otros ingredientes',
  'Tomar un servicio (scoop) de Intenze Nite Preworkout ProScience 20 a 30 minutos antes de iniciar la actividad física.');
  
  -- 20
  insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
 'GoUp','./img/GoUp.png', 2.05, 930, 1, 1,
 'Es un producto alimenticio brinda un complemento en diferentes necesidades proteína aislada de suero de la más alta calidad, acompañada de avena, con múltiples vitaminas y minerales que son necesarios para que todo nuestro cuerpo funcione a la perfección.',
  31, 1, 'Cuchara', 30,
 'Proteína Aislada de Suero WPI, Harina de Avena, Citrato de Calcio, Vainilla (Saborizante Artificial), Espesante (Goma Xanthan 0,5%), Fumarato Ferroso, Edulcorante Natural (Estevia), Omega 3, Ácido Ascórbico, Vitamina B1 (Tiamina), Vitamina B6 (Piridoxina), Vitamina E, Beta Caroteno, Ácido Fólico, Biotina, Yoduro de Potasio, Vitamina K2, Vitamina D3.',
  'Mezclar 1 scoop(31g) en 180ml de agua, agite hasta obtener un producto homogéneo, consúmase una vez preparado.');
  
  -- 21
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
 'Bi-Pro Lite','./img/Bi-Pro Lite.png', 2.4, 26, 1, 2,
 'Bi-Pro Lite es una fórmula nutricional elaborada a base de proteína de suero y caseína, proteínas lácteas con excelentes perfiles nutricionales y diferentes velocidades de absorción, contiene en su fórmula 1.000 mg L-Carnitina, 400 mcg Picolinato de cromo y 5000 mg de colágeno hidrolizado, en las proporciones adecuadas, aporta 26 gramos de proteína por servicio.',
  1, 1, 'Cuchara', 20,
 '5000mg de Colágeno, 1000g de L-Carnitina, 400mcg de Picolinato de cromo.',
  'Mezcle Bi-Pro Lite 54g (1 medida/cucharada) en 300ml de agua o leche descremada. Consúmalo inmediatamente después de entrenar y antes de dormir. Si su entrenamiento es nocturno, úselo después de entrenar y con el desayuno. Para aumento de peso no reemplace ninguna comida con este producto. Incluye medidor (cuchara dosificadora).');
  
  -- 22
  insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
 'ISO CLEAN','./img/ISO_CLEAN.png', 2.0, 40, 1, 2,
 'Iso Clean es la última tecnología en proteínas de suero adicionada con hidroxibetametilbutirato 1000 mg (HMB) y ácido linoleico conjugado 1000 mg (CLA) ideal para personas y atletas con altos requerimientos de proteínas y aminoácidos de la más alta calidad.',
  1.2, 1.2, 'Cuchara', 36,
 'Proteína de suero de leche. (proteína de alta tecnología, contiene Beta-Lactoglabulina, Alfa-Lactoalbúmina, albúmina de suero bovino, inmunoglobulinas y lactoferrina), ácido linoleico conjugado, hidroximetilbutirato de calcio, sabor artificial (vainilla), goma xantano, glucósidos de steviol.',
  'Mezcle ½ medida de ISO CLEAN on 4 onzas de agua o adicione su bebida favorita. Consuma uno o más servicios diarios. Los requerimientos proteicos varían de acuerdo a cada persona, basado en el peso corporal, nivel de actividad y salud general.');
  
  -- 23
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'MEGA CARBS','./img/MEGA_CARBS.png', 2.2, 4, 2, 2,
'MEGA CARBS es un complemento altamente rico en carbohidratos que proporcionan la energía necesaria para personas con alto gasto calórico, en actividades como ciclismo, natación, atletismo entre otros.',
2, 2, 'Cuchara', 20,
'Maltodextrina, Premezcla de aminoácidos de cadena ramificada (L-Leucina, L-Valina, L-Isoleucina),Leche de coco en polvo, L-Glutamina, Ácido Cítrico (Acidulante), Dextrosa Monohidratada, Citrato de Sodio (regular de acidez), Cloruro de Sodio, Fosfato Monoppotásico (regulador de acidez), Goma Xantana (espesante) Sabor artificial (Limón, Coco, Cereza), Sacarina Sódica (edulcorante artificial DMU 2000mg/kg),Ácido Ascórbico, Colorante artificial (azorrubina, amarillo N°5, Azul N°1), Dióxido de Titanio (enturbiante), Contiene tartrazina.',
'Mezcle Mega Carbs, g (2 medidas/cucharadas) en 500 ml de agua. Consúmalo durante el entrenamiento. Incluye medidor (cuchara dosificadora).');

-- 24
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'GLUTA STACK','./img/GLUTA_STACK.png', 1.1, 6, 2, 2,
'Gluta stack, es un producto diseñado para todas aquellas personas que realicen actividad física de alta intensidad, Gluta Stack cuenta con 6.000 mg de glutamina, el aminoácido más degradado durante la actividad física, el consumo de glutamina trae numerosos beneficios ya que desempeña una importante función en el proceso de recuperación muscular.',
3, 3, 'Cuchara', 30,
'Malrodextrina, glutamina, ácido citrico (acidulante), citrato de sodio (regulador de acidez), sabor artificial (frutos rojos : limón, cereza, uva), fosfato monopotásico (regulador de acidez), sacarina sódica (edulcorante artificial DMU 1200mg/kg), ácido ascórbico, sulfato de zinc, dióxido de silicio coloidal (antiaglutinante), color artificial (azorrubina). Alérgenos:Este producto ha sido elaborado en una planta donde se procesa con proteina de leche, proteína de soya, albúmina de huevo y cereales que contienen gluten (trigo, avena).',
'Mezcle 3 medidas de Gluta Stack, con 200ml de agua o adicione su bebida favorita. Consuma uno o más servicios diario.');

-- 25
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'MASS-TECH EXTREME 2000','./img/MASS_TECH_EXTREME_2000.png', 6.0, 2.72, 1, 3,
'El nuevo MASS-TECH es un producto de avanzada para ganar masa muscular diseñado para cualquier persona a quien le cueste trabajo aumentar el tamaño, que esté en fase de voluminización o deseen adquirir incluso más fuerza.',
6, 1, 'Cuchara', 5,
'Complejo de carbohidratos multifásico (polímeros de glucosa, salvado de avena (como Avena sativa), isomaltulosa), sistema de proteínas multifásicas (concentrado de proteína de suero, aislado de proteína de suero 97%, aislado de proteína de suero hidrolizado). ',
'Adultos, tomar 1 servicio (6 Cucharas medidoras) al día. Dividir el servicio  en tres (3) tomas diarias. Disolver cada una en 12 oz. de agua hasta obtener una mezcla homogénea.');

-- 26
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'AMINO BUILD','./img/AMINO_BUILD.png', 1.35, 6.14, 2, 3,
'Amino Build es una fórmula para mejorar los músculos y el rendimiento dosificada científicamente que proporciona a su cuerpo BCAA críticos para aumentar la fuerza y ​​apoyar la recuperación.',
16, 1, 'Cuchara', 40,
'ácido cítrico, ácido málico, dióxido de silicio, sabores naturales, acesulfamo-potasio, sucralosa, betacaroteno (color), antiespuma (polidimetilsiloxano, sílice metilada). Contiene ingredientes de coco. Procesado en una instalación que tambien procesa ingredientes de soja, huevo, pescado, leche, maní, mariscos, frutos secos y trigo.',
'Mezcle 1 porción (1 cucharada) con 8 oz. de agua. Consumir pre-entrenamiento (unos 30 minutos antes) o intra-entrenamiento.');

-- 27
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'PLATINUM 100% GLUTAMINE','./img/PLATINUM_100_GLUTAMINE.png', 66.1, 300, 2, 3,
'La glutamina comprende más del 20% de los aminoácidos del cuerpo y durante el entrenamiento se utiliza para diversas funciones metabólicas, incluida la producción de energía. Una sesión de entrenamiento intensa puede provocar que los músculos pierdan hasta el 50% de sus reservas de glutamina. Por eso hemos diseñado Platinum 100% Glutamina, para ayudar a reponer lo que se pierde durante el ejercicio intenso.',
5, 1, 'Cuchara', 60,
'5gr glutamina, la glutamina comprende más del 20% de los aminoácidos del cuerpo y durante el entrenamiento se utiliza para diversas funciones metabólicas, incluida la producción de energia. Ultra Micronized, la glutamina micronizada permite una absorción más rápida y eficiente en los músculos. HPLC,Cada porción contiene 5g de glutamina premium sin rellenos ni aditivos. Está probado y verificado por HPLC, para que sepa que es puro, potente y eficaz.',
'Toma 1 porción (1 cacito) en agua o tu bebida favorita. Consuma 2 porciones al día para obtener mejores resultados.');

-- 28
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'CELL-TECH','./img/CELL-TECH.png', 3.00, 1.36, 3, 3,
'Cell-Tech es una fórmula de creatina de tercera generación científicamente diseñada que presenta una mezcla clínicamente validada de monohidrato de creatina y carbohidratos.',
50, 1, 'Cuchara', 27,
'Multi-Stage Carb Blend ( Polímeros de Glucosa , Dextrosa , Modcarb™ ( Salvado de Avena , Amaranto, Quinoa, Trigo Sarraceno, Mijo, Chía), Almidón de Maíz Ceroso , Dextrina), Sabores Naturales y Artificiales, Ácido Cítrico , Fosfato Dicálcico , Silicato de Calcio, Sal , Acesulfamo Potásico , Etilcelulosa, Sucralosa , Lecitina de Soja.',
'Tome 1 cucharada con 6 oz. de agua. Para obtener mejores resultados, tome 2 cucharadas con 12 oz. de agua.');
    
-- 29
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'PLATINUM 100% CREATINE','./img/PLATINUM_100_CREATINE.png', 88.1, 400, 3, 3,
'Platinum 100% Creatine proporciona a tus músculos la forma de creatina micronizada más investigada clínicamente disponible.',
5, 1, 'Cuchara', 80,
'5GR CREATINE,las investigaciones han demostrado que la creatina es eficaz para aumentar la masa muscular, mejorar la fuerza y ??mejorar el rendimiento. Platinum 100% Creatine ofrece creatina micronizada de la mejor calidad, y cada porción contiene una dosis recomendada de 5 g. Este producto puede ayudarle a superar sus objetivos de acondicionamiento físico. Ultra Micronized, la creatina micronizada permite una absorción más rápida y eficiente en los músculos. HPLC, la Cromatografía Líquida de Alta Eficiencia (HPLC, por sus siglas en inglés) es una técnica analítica que permite separar mezclas complejas de sustancias de procedencia diversa, con el propósito de identificarlas, cuantificarlas y purificarlas.',
'Durante los primeros 3 días (fase de carga): Mezcle 1 cucharada con 8 oz. de agua y consumir 4 veces al día. Después del día 3 (fase de mantenimiento): Mezcle 1 cucharada con 8 oz. de agua 1 a 2 veces al día. Para obtener resultados más impresionantes, reemplace el agua con una bebida con carbohidratos o una bebida deportiva. Mantener un adecuado estado de hidratación durante su uso.');

-- 30
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'CELL-TECH CREACTOR','./img/CELL_TECH_CREACTOR.png', 52.9, 240, 3, 3,
'Cell-Tech Creactor proporciona clorhidrato de creatina (HCI) sin rellenos, carbohidratos ni grasas',
2, 1, 'Cuchara', 120,
'Goma xantana, ácido cítrico, saborizante natural, silicato de calcio, dióxido de silicio, acesulfamo de potasio, sucralosa. Producto fabricado en instalaciones que procesan leche, huevo, trigo, soya, maní, frutos secos, pescado y mariscos.',
'Mezcle 1 cucharada con 6 a 8 oz. De agua o mezcle 2 cucharadas con 12 a 16 oz. De agua');


-- 31
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'PROTON +','./img/PROTON_+.png', 3.05, 1380, 1, 4,
'Proton + conocida anteriormente como proton + gainer, es una mezcla de proteínas lácteas (Proteína concentrada de leche, Caseina Micelar) con Maltodextrina, harina de avena y otros ingredientes',
230, 6, 'Cuchara', 6,
'Mezcla de proteínas lácteas (Proteína concentrada de leche, Caseina Micelar), Maltodextrina, Extender M, Harina de Avena, Harina de Arroz, Monohidrato de Creatina, Lecitina de Soya, Sabor Cookie Vanilla, HMB, Omega 3, Dióxido de Silica y Sucralosa.',
'Se recomienda seguir las indicaciones de uso proporcionadas por el fabricante para obtener los mejores resultados. Por lo general, se aconseja consumir una porción según las necesidades nutricionales individuales.');

-- 32
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'ALPHA BCAA','./img/ALPHA_BCAA.png', 1.32, 600, 2, 4,
'Alpha BCAA, mezcla en polvo a base de fruta micropulverizada',
220, 1, 'Cuchara', 30,
'6g de aminoácidos ( 3 L- Leucina, 1.5 L-isoleucina, 1.5 L-valina), 5g de glutamina Vegana, Cloruros de potasio, magnesio y sodio, en una base de fruta natural micropulverizada',
'El mejor momento para consumir alpha bcaas es durante el entrenamiento  lo que nos va a ayudar a proteger nuestra masa muscular del catabolismo muscular  en los entrenos de larga duración o entrenos de alto esfuerzo.');

-- 33
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'ELECTRON 2.0','./img/ELECTRON_2.0.png', 1.32, 600, 5, 4,
'ELECTRON es una partícula subatómica que al igual que en la física que transporta energía de un polo negativo a un polo positivo. Un Pre- Entreno con 15 ingredientes que generan energía, vasodilatación y concentración, dando como resultado una experiencia explosiva a tu entrenamiento. Bajo en azúcares, grasas y carbohidratos y en un exclusivo sabor a ICE BERRY.',
20, 1, 'Cuchara', 25,
'L- citrulina, beta alanina, arginina, glicina, taurina, tirosina, cafeína anhidra, ácido de niacina, betaína (extracto de remolacha) – presente como extracto de remolacha en polvo, polvo de fruta, dióxido de silicio, sucralosa',
'Consumir 1 scoop en 200 Ml de agua 30-40 min antes de la actividad física.');

-- 34
insert into Suplementos(nombre, foto, peso_lb, peso_g, categoria_id, marca_id, descripcion, tamano_x_porcion, medida_x_porcion, tipo_medida, porciones_x_envase, ingredientes, modo_empleo) values (
'REACTION','./img/REACTION.png', 0.79, 360, 4, 4,
'Reaction es un alimento en polvo a base de fruta micropulverizada, que contiene ingredientes como extracto de cafe verde – ⁠teanina – ⁠pimienta negra – ⁠pimienta cayera – ⁠L-carotina – ⁠cafeína – ⁠té verde – ⁠extracto de brócoli, entre otros.',
6, 1, 'Cuchara', 60,
'Extracto de cafe verde,⁠teanina, ⁠pimienta negra, ⁠pimienta cayera,  ⁠L-carotina,  ⁠cafeína, té verde, ⁠extracto de brócoli, ⁠extracto de mango, ⁠extracto de naranja amarga, ⁠CLA.',
'Se sugiere seguir las indicaciones del fabricante para obtener los mejores resultados. Por lo general, se mezcla una porción del producto con agua y se consume antes del entrenamiento.');




-- Nutrientes_x_suplemento
	-- 1
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (1, 1, 108, 'KCal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (1, 2, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (1, 3, 0, 'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (1, 4, 0, 'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (1, 5, 0, 'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (1, 6, 5, 'mg', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (1, 7, 95, 'mg', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (1, 8, 1, 'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (1, 9, 0, 'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (1, 10, 26, 'g', 52);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (1, 11, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (1, 12, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (1, 13, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (1, 14, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (1, 15, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (1, 16, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (1, 17, 100);
	-- 2
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (1, 18, 3000, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (2, 1, 15);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (2, 2, 0, 'KCal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (2, 3, 0, 'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (2, 4, 0, 'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (2, 6, 0, 'mg', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (2, 7, 100, 'mg', 4);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (2, 8, 4, 'g', 1);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (2, 19, 0, 'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (2, 9, 0, 'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (2, 10, 0, 'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (2, 11, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (2, 17, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (2, 13, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (2, 12, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (2, 20, 4000, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (2, 18, 4000, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (2, 21, 2000, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (2, 22, 2000, 'mg');
	-- 3
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (3, 1, 10, 'Kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (3, 2, 0, 'Kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (3, 3, 0, 'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (3, 4, 0, 'mg', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (3, 6, 0, 'mg', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (3, 7, 80, 'mg', 3);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (3, 23, 50, 'mg', 1);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (3, 8, 3, 'g', 1);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (3, 19, 0, 'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (3, 9, 0, 'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (3, 10, 0, 'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (3, 11, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (3, 24, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (3, 17, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (3, 12, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (3, 25, 1000, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (3, 26, 50, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (3, 27, 20, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (3, 28, 3400, 'mg');

	-- 4 
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 11, 3000, 'mcg', 333);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 17, 135, 'mg', 150);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 16, 10, 'mcg', 50);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 29, 40.5, 'mg', 270);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 30, 20, 'mg', 1667);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 31, 13.5, 'mg', 1038);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 24, 60, 'mg', 375);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 14, 10, 'mg', 588);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 32, 510, 'mcg DFE', 128);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 33, 100, 'mcg', 4167);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 34, 165, 'mcg', 550);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 35, 80, 'mg', 1600);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 36, 10, 'mg', 2);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 13, 160, 'mg', 12);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 37, 145, 'mg', 35);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 38, 9.5, 'mg', 86);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 39, 1, 'mg', 111);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 40, 7, 'mg', 304);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 41, 10, 'mcg', 22);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 7, 10, 'mg', 1);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (4, 23, 35, 'mg', 1);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 42, 865, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 26, 49, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 43, 2.5, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 44, 2.5, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 45, 2.5, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 46, 2.5, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 47, 390, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 48, 50, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 49, 25, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 50, 20, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 51, 20, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 52, 20, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 53, 5, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 54, 5, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 55, 1.25, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 56, 72, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 57, 14, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 58, 13, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 59, 10, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 60, 10, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 61, 25, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (4, 62, 25, 'mg');

	-- 5
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (5, 1, 15, 'Kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (5, 2, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (5, 3, 0, 'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (5, 4, 0, 'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (5, 5, 0, 'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (5, 6, 0, 'mg', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (5, 7, 190, 'mg', 8);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (5, 8, 4, 'g', 1);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (5, 19, 2, 'g', 8);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (5, 9, 1, 'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (5, 10, 0, 'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (5, 11, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (5, 12, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (5, 23, 6);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (5, 37, 15);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (5, 13, 50);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (5, 17, 90);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (5, 38, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (5, 20, 4000, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (5, 18, 3500, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (5, 21, 1750, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (5, 22, 1750, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (5, 63, 885, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (5, 64, 520, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (5, 65, 335, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (5, 66, 140, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (5, 43, 75, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (5, 67, 45, 'mg');

    -- 6
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (6, 1, 5, 'Kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (6, 2, 5, 'Kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (6, 3, 0.5, 'g', 1);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (6, 4, 0, 'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (6, 6, 0, 'mg', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (6, 7, 70, 'mg', 3);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (6, 8, 0, 'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (6, 19, 0, 'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (6, 9, 0, 'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (6, 10, 0, 'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (6, 11, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (6, 13, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (6, 68, 21);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (6, 17, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (6, 12, 0);

 -- 7
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (7, 3, 1,'g', 2);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (7, 4, 0.5,'g', 3);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (7, 6, 45,'mg', 15);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (7, 7, 110,'mg', 5);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (7, 8, 20,'g', 7);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (7, 19, 2,'g', 8);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (7, 9, 1,'g', 3);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (7, 10, 30,'g', 60);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (7, 11, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (7, 17, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (7, 13, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (7, 69, 1000,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (7, 70, 400,'mcg');

   -- 8
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (8, 3, 0,'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (8, 4, 0,'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (8, 5, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (8, 6, 0,'mg', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (8, 7, 5,'mg', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (8, 8, 3,'g', 1);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (8, 19, 1,'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (8, 9, 2,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (8, 10, 0,'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (8, 11, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (8, 17, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (8, 13, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (8, 12, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (8, 72, 5000,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (8, 71, 3000,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (8, 73, 3000,'mg');


 -- 9
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (9, 1, 10,'kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (9, 2, 10,'kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (9, 3, 1,'g', 2);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (9, 4, 0,'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (9, 74, 1,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (9, 4, 0,'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (9, 74, 1,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (9, 75, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (9, 76, 1000,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (9, 77, 250,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (9, 78, 250,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (9, 79, 250,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (9, 80, 125,'mg');

 -- 10 pendiente
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (10, 8, 1,'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (10, 24, 15,'mg', 94);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (10, 36, 40,'mg', 7);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (10, 7, 5,'mg', 1);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (10, 23, 5,'mg', 1);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (10, 81, 1250,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (10, 82, 1600,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (10, 83, 500,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (10, 84, 750,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (10, 85, 200,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (10, 86, 190,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (10, 87, 62.5,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (10, 88, 25,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (10, 88, 25,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (10, 89, 20,'mg');

 -- 11
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (11, 1, 0,'kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (11, 90, 0,'kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (11, 3, 0,'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (11, 4, 0,'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (11, 6, 0,'mg', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (11, 6, 0,'mg', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (11, 7, 95,'mg', 4);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (11, 7, 0,'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (11, 9, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (11, 10, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (11, 11, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (11, 12, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (11, 13, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (11, 15, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (11, 14, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (11, 38, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (11, 17, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (11, 93, 6000,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (11, 94, 3000,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (11, 95, 3000,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (11, 96, 3000,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (11, 97, 2500,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (11, 18, 2000,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (11, 98, 1500,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (11, 21, 1000,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (11, 22, 1000,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (11, 99, 210,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (11, 86, 200,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (11, 100, 200,'mg');

 -- 12
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (12, 1, 20,'kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (12, 90, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 3, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (12, 4, 0,'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (12, 5, 0,'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (12, 6, 0,'mg', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (12, 23, 60,'mg', 2);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (12, 8, 5,'g', 2);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (12, 19, 4,'g', 16);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (12, 19, 4,'g', 16);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (12, 9, 1,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (12, 10, 0,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 11, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 17, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 12, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 30, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 31, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 24, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 35, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 101, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 102, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 34, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 16, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 29, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 103, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 37, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 37, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 39, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 39, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 104, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 105, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 105, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 38, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 106, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (12, 23, 100); 
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (12, 107, 100,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (12, 108, 100, 'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (12, 109, 100, 'mg');

  -- 13
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (13, 1, 120);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (13, 90, 20);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (13, 3, 2,'g', 3);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (13, 4, 1,'g', 5); 
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (13, 6, 65,'mg', 22); 
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (13, 7, 120,'mg', 5);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (13, 8, 2,'g', 1);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (13, 9, 2,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (13, 10, 24,'g', 48);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (13, 13, 10);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (13, 12, 2);

  -- 14
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (14, 1, 130);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (14, 90, 20);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (14, 3, 2,'g', 3);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (14, 4, 1,'g', 5);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (14, 5, 0,'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (14, 6, 70,'mg', 23);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (14, 7, 70,'mg', 3);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (14, 8, 2,'g', 1);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (14, 19, 0,'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (14, 6, 70,'mg', 23);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (14, 9, 1,'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (14, 10, 25,'g', 50);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (14, 17, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (14, 13, 15);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (14, 11, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (14, 12, 0);

  -- 15
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (15, 1, 87,'kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (15, 3, 0.7,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (15, 4, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (15, 8, 1.0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (15, 19, 0.1,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (15, 9, 0.4,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (15, 110, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (15, 10, 21,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (15, 7, 46,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (15, 13, 116,'mg');

 -- 16
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (16, 2, 90,'kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (16, 3, 10,'g',15);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (16, 4, 0,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (16, 5, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (16, 6, 30,'mg',10);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (16, 7, 60,'mg',3);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (16, 8, 166,'g',55);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (16, 19, 13,'g',52);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (16, 111, 3,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (16, 112, 8,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (16, 113, 1,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (16, 10, 44,'g',88);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (16, 11, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (16, 17, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (16, 15, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (16, 14, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (16, 16, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (16, 38, 100);

-- 17
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (17, 2, 18);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (17, 3, 2,'g', 3);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (17, 4, 0,'g', 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (17, 5, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (17, 6, 0,'mg',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (17, 7, 15,'mg',1);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (17, 8, 1,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (17, 19, 0,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (17, 113, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (17, 10, 25,'g',50);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (17, 11, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (17, 17, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (17, 29, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (17, 16, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (17, 34, 100);

-- 18
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (18, 1, 26);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (18, 8, 6.5);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (18, 19, 3.0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (18, 114, 0.6);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (18, 110, 0.4);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (18, 7, 3.9);

-- 19
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (19, 1, 108,'Kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (19, 90, 0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (19, 3, 0,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (19, 4, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (19, 5, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (19, 6, 5,'mg',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (19, 7, 95,'mg',4);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (19, 8, 1,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (19, 103, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (19, 10, 26,'g',52);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (19, 11,0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (19, 12,0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (19, 13,0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (19, 14,100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (19, 15,100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (19, 16,100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (19, 16,100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (19, 17,100);

-- 20
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (20, 1, 120,'Kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (20, 90, 10);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (20, 3, 1,'g',2);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (20, 4, 0,'g',3);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (20, 5, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (20, 115, 20,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (20, 6, 0,'mg',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (20, 7, 75,'mg',3);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (20, 8, 7,'g',5);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (20, 19, 4,'g',16);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (20, 113, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (20, 10, 4,'g',16);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (20, 13, 10);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (20, 106, 20);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (20, 34, 25);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (20, 11, 30);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (20, 12, 40);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (20, 116, 50);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (20, 29, 50);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (20, 117, 75);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (20, 15, 90);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (20, 14, 90);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (20, 17, 100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (20, 118, 100);

-- 21
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (21, 1, 190,'Kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (21, 3, 0.5,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (21, 4, 0.2,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (21, 8, 26,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (21, 19, 2.1,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (21, 114, 2.0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (21, 110, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (21, 10, 26,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (21, 7, 84,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (21, 13, 289,'mg');

-- 22
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (22, 1, 102,'kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (22, 3, 2.1,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (22, 4, 0.6,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (22, 8, 1.3,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (22, 19, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (22, 114, 1.0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (22, 110, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (22, 10, 20,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (22, 7, 41,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (22, 12, 130,'mg');

-- 23
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (23, 1, 175,'kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (23, 3, 1.8,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (23, 4, 0.9,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (23, 8, 39,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (23, 114, 2.0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (23, 110, 1.0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (23, 10, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (23, 7, 143,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (23, 23, 50,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (23, 17, 60,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (23, 20, 2000,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (23, 21, 1000,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (23, 18, 2000,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (23, 22, 1000,'mg');

-- 24
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (24, 1, 31,'kcal');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (24, 3, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (24, 4, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (24, 8, 7.7,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (24, 19, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (24, 9, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (24, 10, 0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (24, 7, 29,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (24, 23, 72,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (24, 17, 83,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (24, 28, 11,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (24, 71, 6000,'mg');

-- 25
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25,1,7,'g',9);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25,6,180,'mg',60);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25,10,80,'g',160);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25,11,740,'mcg',82);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25,17,30,'mg',33);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25,16,4.9,'mcg',25);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 29,11.1,'mg',74);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 103,15,'mcg',13);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 30,1.8,'mg',150);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 31,2.6,'mg',200);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 24,32.3,'mg',202);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 14,1.7,'mg',100);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 32,360,'mcg',90);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 33,6,'mcg',250);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 34,157,'mcg',523);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 119,6.8,'mg',136);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 13,1.150,'mg',88);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 12,14.4,'mg',80);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 120,79,'mcg',53);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 38,8.6,'mg',78);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 105,33,'mcg',60);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 39,1.7,'mg',189);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 68,156,'mcg',446);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 7,990,'mg',43);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (25, 23,1.510,'mg',32);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (25, 71,15.2,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (25, 28,5,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (25, 21,8.3,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (25, 22,4.8,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (25, 21,4.4,'g');

-- 26
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (26, 37,50,'mg',12);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (26, 38,15,'mg',136);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (26, 7,170,'mg',7);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (26, 23,50,'mg',1);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (26,18,4,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (26,21,1.5,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (26,22,1.5,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (26,121,2.5,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (26,83,1,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (26,122,595,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (26,123,100,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (26,124,100,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (26,125,80,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (26,126,45,'mg');

-- 27
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (27, 20,5,'g');

-- 28 
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (28,1,150);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (28,8,38,'g',14);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (28,19,2,'g',7);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (28,114,14,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (28,110,14,'g',28);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (28,17,125,'mg',139);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (28,14,5.3,'mg',312);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (28,33,0.2,'mcg',8);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (28,37,30,'mg',7);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (28,7,45,'mg',2);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (28,23,30,'mg',1);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (28,28,5,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (28,83,1,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (28,94,500,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (28,18,500,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (28,22,250,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (28,21,250,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (28,127,100,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (28,126,55,'mg');

-- 29
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (29,28,5,'g');

-- 30
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (30,3,0,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (30,8,0,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (30,7,10,'mg',1);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (30,128,750,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (30,129,750,'mg');

-- 31
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (31,1,860);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (31,2,50);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (31,3,6,'g',9);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (31,4,1.5,'g',8);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (31,5,0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (31,6,110,'mg',37);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (31,7,110,'mg',5);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (31,8,151,'g',50);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (31,19,3,'g',12);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (31,113,8,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (31,10,50,'g',98);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (31,11,0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (31,17,0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (31,12,25);

-- 32
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (32,1,12);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, porcentaje_valor_diario) values (32,2,0,0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (32,3,0,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (32,4,0,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (32,5,0,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (32,6,0,'mg',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (32,7,5,'mg',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (32,8,3,'g',1);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (32,19,1,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (32,113,2,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (32,10,0,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (32,110,0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (32,13,0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (32,17,0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (32,12,0);

-- 33
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (33,1,15);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (33,2,0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (33,3,0,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (33,4,0,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (33,5,0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (33,6,0,'mg',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (33,7,5,'mg',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (33,19,1,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (33,113,2,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (33,10,0,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades, porcentaje_valor_diario) values (33,3,0,'g',0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (33,11,0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (33,17,0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (33,13,0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, porcentaje_valor_diario) values (33,12,0);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (33,93,5000,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (33,94,3000,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (33,95,1500,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (33,42,1300,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (33,130,750,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (33,131,250,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor, nutriente_unidades) values (33,86,200,'mg');

-- 34
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor) values (34,1,11);
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor,nutriente_unidades) values (34,3,0.3,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor,nutriente_unidades) values (34,4,0.0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor,nutriente_unidades) values (34,5,0.0,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor,nutriente_unidades) values (34,8,2.1,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor,nutriente_unidades) values (34,19,0.2,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor,nutriente_unidades) values (34,114,0.3,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor,nutriente_unidades) values (34,110,0.3,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor,nutriente_unidades) values (34,10,0.0,'g');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor,nutriente_unidades) values (34,7,0.3,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor,nutriente_unidades) values (34,13,1.3,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor,nutriente_unidades) values (34,12,0.08,'mg');
insert into Nutrientes_x_suplemento(suplemento_id, nutriente_id, nutriente_valor,nutriente_unidades) values (34,38,0.03,'mg');

-- Usuarios
insert into Usuarios(nombres, apellidos, telefono, email, contrasena) values ('María Camila', 'García Torres', '3116752324', 'macamilag@gmail.com', '$2y$10$eRJ1KCTFC.kv1hxG9Eyqy.eXRbJVpTOnlv3LmvGMFNAMhzWaHEZH2');
insert into Usuarios(nombres, apellidos, telefono, email, contrasena) values ('Juan Felipe', 'Moreno Sandoval', '3204567867', 'juanpipe10@gmail.com', '$2y$10$CRs68w0Af5CP6WXWnaFNEePJDSO1xjHTO2ZfA1jLFK/xpNhVGhXka');
insert into Usuarios(nombres, apellidos, telefono, email, contrasena) values ('Nicolás', 'Méndez Rodríguez', '3053335235', 'nickmen@gmail.com', '$2y$10$zeYNAq6OPUwvBjmGPkS2kedXy/weKJa65FGK.gRrtlUqVcBVUVfNi');
insert into Usuarios(nombres, apellidos, telefono, email, contrasena) values ('Ana María', 'Cárdenas Arango', '3152343465', 'nanacg90@gmail.com', '$2y$10$84A2ULYpu1gaTiy874k9Ge10sBfJGuEGEMCtk5VMEtxOHhK7qVHjO');
insert into Usuarios(nombres, apellidos, telefono, email, contrasena) values ('Luz Catherine Daniela', 'Amado Torres', '3208437425', 'amado_torres@gmail.com', '$2y$10$WOHAqDNWUKePjH7Pm1n34.t8kpIL149PwTgbVIN3pZ6/1OYCjEGKG');
insert into Usuarios(nombres, apellidos, telefono, email, contrasena) values ('Juan Camilo', 'Pineda Ortiz', '3208343458', 'juan_pineda@gmail.com', '$2y$10$3TTn58qvhIbnY4lAVPRsq.6cJAEUK1V5qbGPb5B6GjjRPBq7VwFTm');	

-- Favoritos
insert into Favoritos (usuario_id, suplemento_id) values 
(1, 6),
(2, 5),
(3, 4),
(4, 3),
(5, 2),
(6, 1);