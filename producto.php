<?php
    include_once "header/header_index.php";
    include "includes/producto.inc.php";

    $servingSize = $producto->fetchProductServingSize();
    $servingMeasure = $producto->fetchProductServingMeasure();
    $measureType = $producto->fetchProductMeasureType();

    $nutrientes = $producto->fetchProductNutrients();
    $nNutrientes = $producto->fetchProductsNutrientCount();

    $nutrientesIniciales = array("Calorías", "Calorías grasa", "Calorías de la grasa");
    $nutrientesCentrales = array("Grasa Total", "Grasa Saturada", "Grasa Trans", "Colesterol", "Sodio", "Carbohidratos Totales", "Fibra Dietaria", "Azúcares", "Azucares añadidos", "Proteína");

    function compararArreglo(string $nutriente, array $_arregloNutrientes) {
        foreach ($_arregloNutrientes as $value) {
            if ($nutriente == $value) {
                return true;
            }
        }
    }
?>

<head>
    <title><?php $producto->printProductName()?> | Guía de Suplementos Deportivos</title>
    <link rel="stylesheet" href="css/styles_producto.css">
    <!-- <title>Product Details</title> -->
</head>

<section id="product">
        <div class="container">
            <div class="row">
                <div class="col-lg-5" data-aos="fade-right" data-aos-duration="2000">
                    <div class="cont-foto position-relative overflow-hidden h-100 d-flex flex-column justify-content-center align-items-center">
                        <img src="<?php $producto->printProductFoto() ?>" alt="Foto del suplemento">
                    </div>
                </div>
                <div class="col-lg-7 cont-info" data-aos="fade-left" data-aos-duration="2000">
                    <h1><?php $producto->printProductName()?> - <?php $producto->printProductBrand() ?></h1>
                    <span class="badge mt-2 custom-badge">
                    <?php $producto->printProductCategory() ?>
                    </span>
                    <span>
                        <i class="<?= $producto->verificarFavorito() ? 'fa-solid' : 'fa-regular' ?> fa-heart guardar-favorito" data-producto="<?php echo $producto->fetchProductId(); ?>"></i>
                    </span>
                    <script>
                        document.addEventListener('DOMContentLoaded', function () {
                        // Espera a que el DOM esté completamente cargado antes de ejecutar el código
                        var corazon = document.querySelector('.guardar-favorito');
                        // Busca el elemento que tiene la clase 'guardar-favorito'
                        if (corazon) { // Verifica si se encontró un elemento
                            corazon.addEventListener('click', function (event) {
                                // Agrega un evento de clic al elemento
                                var idProducto = this.getAttribute('data-producto');
                                // Obtiene el valor del atributo 'data-producto' del elemento, que contiene el ID del producto
                                window.location.href = 'includes/guardar-fav.inc.php?producto=' + idProducto + '&from=producto';
                                // Redirige al usuario a 'guardar_favorito.php' con el ID del producto como parámetro en la URL
                            });
                        }
                    });
                    </script> 
                    <!-- Accordion -->
                    <div class="accordion pt-4" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Descripción
                            </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse show" data-bs-parent="#accordionExample">
                                <div class="accordion-body"><?php $producto->printProductDescription() ?></div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Tabla nutricional
                                <?php 

                                ?>

                                <!-- Script para actualizar la información de la página dependiendo de la opción seleccionada 
                                <script>
                                    function updateInfo(select) {
                                        var selectedIndex = select.selectedIndex;
                                        var infoDiv = document.getElementById("info");
                                        var options = select.options;

                                        // Obtener la opción seleccionada y su valor
                                        var selectedOption = options[selectedIndex];
                                        var selectedValue = selectedOption.value;

                                        // Actualizar la información dinámica
                                        infoDiv.innerHTML = selectedOption.innerHTML;
                                        
                                        // Si deseas hacer alguna acción adicional basada en el valor seleccionado, puedes hacerlo aquí. Por ejemplo:
                                        if (selectedValue === "0.5") {
                                            <?php
                                                $producto->cambiarValores(1);
                                            ?>
                                        } else if (selectedValue === "2") {
                                            <?php
                                                $producto->cambiarValores(2);
                                            ?>
                                        }
                                    }
                                    
                                    // Llamar a la función para que se ejecute inicialmente con la opción seleccionada por defecto
                                    updateInfo(document.querySelector('.form-select'));
                                </script> -->
                            </button>
                            </h2>
                            <div id="collapseTwo" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
                                <div class="accordion-body nutrifacts-container">
                                    <?php
                                        if ($measureType == "Tabletas" || $measureType == "Cápsulas") {
                                            $nTabletas = $producto->fetchProductNoTabletas();
                                            ?>
                                                <div class="pesos-container">
                                                    <a class="btn disabled placeholder col-4" aria-disabled="true"><strong>Número de tabletas:</strong> <?php echo $nTabletas ?></a>
                                                </div> 
                                            <?php
                                        } else {
                                            $ese = '';
                                            if ($servingMeasure > 1) {
                                                $ese = 's';
                                            }
                                            ?>
                                            <div class="pesos-container">
                                                <a class="btn disabled placeholder col-4" aria-disabled="true"><strong>Peso en lb:</strong> <?php $producto->printProductLbWeight() ?></a>
                                            </div> 
                                            <div class="pesos-container">
                                                <a class="btn disabled placeholder col-4" aria-disabled="true"><strong>Peso en g:</strong> <?php $producto->printProductGWeight() ?></a>
                                            </div> 
                                            <?php
                                        }
                                    ?>
                                    <div class="nutrifacts">
                                        <!-- <span>Tamaño de Porción: 33g 1 cuchara</span> -->
                                        <div class="select-container">
                                            <?php
                                                if ($measureType == "Tabletas" || $measureType == "Cápsulas") {
                                                    ?>
                                                    <span>Tamaño de Porción: <?php echo $servingSize ?> - <?php echo $measureType ?></span>
                                                    <!--<select class="form-select" aria-label="Default select example">  onchange="updateInfo(this)" -->
                                                        <!-- <option value="0.5">Tamaño de Porción: <?php //echo $servingSize/2 ?> - <?php //echo $measureType ?></option>
                                                        <option selected>Tamaño de Porción: <?php //echo $servingSize ?> - <?php //echo $measureType ?></option>
                                                        <option value="2">Tamaño de Porción: <?php //echo $servingSize*2 ?> - <?php //echo $measureType ?></option>
                                                    </select> -->
                                                    <?php
                                                } else {
                                                    $ese = '';
                                                    if ($servingMeasure > 1) {
                                                        $ese = 's';
                                                    }
                                                    ?>
                                                    <span>Tamaño de Porción: <?php echo $servingSize ?>g - <?php echo $servingMeasure ?> <?php echo $measureType . $ese ?></span>
                                                    <!--<select class="form-select" aria-label="Default select example">  onchange="updateInfo(this)" -->
                                                        <!-- <option value="0.5">Tamaño de Porción: <?php //echo $servingSize/2 ?>g - <?php //echo $servingMeasure/2 ?> <?php //echo $measureType . $ese ?></option>
                                                        <option selected>Tamaño de Porción: <?php //echo $servingSize ?>g - <?php //echo $servingMeasure ?> <?php //echo $measureType . $ese ?></option>
                                                        <option value="2">Tamaño de Porción: <?php //echo $servingSize*2 ?>g - <?php //echo $servingMeasure*2 ?> <?php //echo $measureType ?>s</option>
                                                    </select> -->
                                                    <?php
                                                }
                                            ?>
                                            
                                        </div>
                                        <!-- <br> -->
                                        <span>Porciones por envase: <?php $producto->printProductServingsPerContainer() ?></span>
                                        <div class="separator"></div>
                                        <table>
                                            <!-- tabla de 2 columnas -->
                                            <tr>
                                                <td colspan="2"> <!-- propiedad para indicar cuantas columnas abarca una celda -->
                                                    <strong>Cantidad de Porción</strong>
                                                </td>
                                            </tr>

                                            <?php 
                                                for ($i=0; $i < $nNutrientes; $i++) { 
                                                    /* if ($nutrientes[$i]["nutriente_nombre"] == "Calorías" || $nutrientes[$i]["nutriente_nombre"] == "Calorías grasa") { */
                                                    if (compararArreglo($nutrientes[$i]["nutriente_nombre"], $nutrientesIniciales)) {
                                                        ?>
                                                            <tr>
                                                                <td colspan="2"><strong><?php $producto->printNutrientName($i) ?>:</strong> <?php $producto->printNutrientValue($i); echo " ";$producto->printNutrientUnit($i) ?></td>
                                                            </tr>
                                                        <?php
                                                    }
                                                    else {
                                                        # code...
                                                    }
                                                }
                                            ?>
                                            <tr>
                                                <td></td>
                                                <td class="align-text-right"><strong>% Valor Diario</strong></td>
                                            </tr>
                                        
                                            <?php 
                                                for ($i=0; $i < $nNutrientes; $i++) { 
                                                    if (compararArreglo($nutrientes[$i]["nutriente_nombre"], $nutrientesCentrales)) {
                                                        if ($nutrientes[$i]["nutriente_nombre"] == "Grasa Total" || $nutrientes[$i]["nutriente_nombre"] == "Colesterol" || $nutrientes[$i]["nutriente_nombre"] == "Sodio" || $nutrientes[$i]["nutriente_nombre"] == "Carbohidratos Totales" || $nutrientes[$i]["nutriente_nombre"] == "Proteína") {
                                                        ?>
                                                            </tr>
                                                            <td><strong><?php $producto->printNutrientName($i) ?></strong> <?php $producto->printNutrientValue($i) ?>g</td>
                                                            <td class="align-text-right"><?php $producto->printNutrientPercentage($i) ?>%</td>
                                                            </tr>
                                                        <?php
                                                        } else {
                                                        ?>
                                                            </tr>
                                                            <td><?php $producto->printNutrientName($i); echo " "; $producto->printNutrientValue($i) ?>g</td>
                                                            <td class="align-text-right"><?php $producto->printNutrientPercentage($i) ?>%</td>
                                                            </tr>
                                                        <?php
                                                        }
                                                    }
                                                }
                                            ?>
                                        </table>
                                        <div class="separator"></div>
                                        <!-- Tabla de "demás" ingredientes (vitaminas, minerales...) -->
                                        <table>

                                        <?php 
                                                for ($i=0; $i < $nNutrientes; $i++) { 
                                                    if (!compararArreglo($nutrientes[$i]["nutriente_nombre"], $nutrientesIniciales) && !compararArreglo($nutrientes[$i]["nutriente_nombre"], $nutrientesCentrales)) {
                                                        if (isset($nutrientes[$i]["porcentaje_valor_diario"])) {
                                                        ?>
                                                            <tr>
                                                                <td><?php $producto->printNutrientName($i) ?></td>
                                                                <td class="align-text-right"><?php $producto->printNutrientPercentage($i) ?>%</td>
                                                            </tr>
                                                        <?php
                                                        } else {
                                                        ?>
                                                            <tr>
                                                                <td><?php $producto->printNutrientName($i) ?></td>
                                                                <td class="align-text-right"><?php $producto->printNutrientValue($i); $producto->printNutrientUnit($i) ?></td>
                                                            </tr>
                                                        <?php
                                                        }
                                                        ?>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Ingredientes
                            </button>
                            </h2>
                            <div id="collapseThree" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
                            <div class="accordion-body"><?php $producto->printProductIngredients() ?></div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Modo de uso
                            </button>
                            </h2>
                            <div id="collapseFour" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
                            <div class="accordion-body"><?php $producto->printProductUsage() ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Sección pie de pagina-->
    <footer>
    <div class="useful-links">
        <ul>
            <img src="img/logo_blanco.png" class="mb-3" width="100" alt="Logo de la pagina">
            <li><a href="index_log.php">Inicio</a></li>
            <li><a href="https://sanmonor.github.io/Proyecto_Suplementos_Deportivos/" target="_blank"
                    rel="noopener noreferrer">Nuestro Portafolio</a></li>
            <li><a href="#">Términos y condiciones</a></li>
        </ul>
    </div>
    <div class="copyright">
        <p>&copy; 2023 Guía de suplementos deportivos. Todos los derechos reservados.</p>
    </div>
    </footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>
</body>
</html>