<?php
session_start();
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="img/favicon1.ico">
    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <!-- CSS -->
    <link rel="stylesheet" href="css/styles.css">
    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/ab4a86798a.js" crossorigin="anonymous"></script>
    <!-- Iconos Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
    <!-- Libreria JavaScript Sweetalert2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>

<body>
    <!-- Barra de navegacion -->
    
    <nav class="navbar navbar-expand-lg seccion-oscura navbar-light py-auto fixed-top ">
        <div class="container"> 
            <a class="navbar-brand d-flex justify-content-between align-items-center order-lg-0" href="index.php">
                <img src="img/logo_blanco.png" width="100" alt="Logo de la pagina">
            </a>

            <div class="order-lg-2 nav-btns d-flex align-items-center">
                <div class="dropdown">
                    <a class="btn position-relative dropdown-toggle text-white" href="#" role="button"
                        id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        
                        <?php
                            if (isset($_SESSION["correo_session"])) {
                                ?>
                                <?php echo $_SESSION["nombre_session"]; ?>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <li><a class="dropdown-item" href="profile.php">Mi cuenta</a></li>
                            <li><a class="dropdown-item" href="includes/logout.inc.php">Cerrar sesión</a></li>
                        <?php
                            } else {
                        ?>
                            <i class="fa-regular fa-user"></i>
                        </a>

                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <li><a class="dropdown-item" href="login.php">Inicia sesión</a></li>
                            <li><a class="dropdown-item" href="registro.php">Regístrate</a></li>
                        <?php
                            }
                        ?>
                    </ul>
                </div>
                
                <!-- Favoritos -->
                <?php
                    if (isset($_SESSION["correo_session"])) {
                ?>
                    <button type="button" class="btn position-relative mx-2 text-white" onclick="window.location.href = 'favoritos.php';">
                        <i class="fa-regular fa-heart"></i>
                    </button>
                <?php
                    } else {
                ?>
                    <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-placement="bottom" data-bs-content="¡Regístrate o inicia sesión para disfrutar de todo nuestro contenido!">
                        <button type="button" class="btn position-relative mx-2 text-white">
                            <i class="fa-regular fa-heart"></i>
                        </button>
                    </span>
                <?php
                    }
                ?>

                <!-- Búsqueda -->
                <?php
                    if (isset($_SESSION["correo_session"])) {
                ?>
                    <form id="search-form" class="d-flex" role="search" action="catalogo.php" method="GET">
                        <div class="form-group">
                            <input type="text" class="form-control me-2" id="recuadro_buscar" name="palabraClave" placeholder="Buscar"
                                aria-label="Search">
                        </div>
                        <button type="button" id="btn_buscar" class="btn text-white"><i class="fa-solid fa-magnifying-glass"></i></button>
                    </form>

                    <script>
                        // Evento click al botón buscar
                        document.getElementById("btn_buscar").addEventListener("click", function () {
                            // Obtener el valor del input de búsqueda
                            var palabraClave = document.getElementById("recuadro_buscar").value;

                            if (palabraClave) {
                                // Redirigir a catalogo.php con la palabra clave como parámetro en la URL
                                window.location.href = 'catalogo.php?palabraClave=' + encodeURIComponent(palabraClave);
                            }
                        });
                    </script>
                <?php
                    } else {
                ?>
                    <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-placement="bottom" data-bs-content="¡Regístrate o inicia sesión para disfrutar de todo nuestro contenido!">
                        <button type="button" class="btn position-relative mx-2 text-white">
                            <i class="fa-solid fa-magnifying-glass"></i>
                        </button>
                    </span>
                <?php
                    }
                ?>
            </div>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse order-lg-1" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto text-center">
                    <li class="nav-item px-2 py-2">
                        <a class="nav-link text-white" href="index.php">Inicio</a>
                    </li>
                    <li class="nav-item dropdown px-2 py-2">
                        <a class="nav-link dropdown-toggle text-white" href="#" role="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            Categorías
                        </a>
                        <ul class="dropdown-menu">
                            <?php
                                if (isset($_SESSION["correo_session"])) {
                            ?>
                                <button type="button" id="btn_proteinas" class="dropdown-item"
                                onclick="window.location.href = 'catalogo.php?palabraClave=<?= 'Proteínas' ?>'">Proteínas</button>
                                <button type="button" id="btn_proteinas" class="dropdown-item"
                                    onclick="window.location.href = 'catalogo.php?palabraClave=<?= 'Aminoácidos' ?>'">Aminoácidos</button>
                                <button type="button" id="btn_proteinas" class="dropdown-item"
                                    onclick="window.location.href = 'catalogo.php?palabraClave=<?= 'Creatinas' ?>'">Creatinas</button>
                                <button type="button" id="btn_proteinas" class="dropdown-item"
                                    onclick="window.location.href = 'catalogo.php?palabraClave=<?= 'Quemadores' ?>'">Quemadores</button>
                                <button type="button" id="btn_proteinas" class="dropdown-item"
                                    onclick="window.location.href = 'catalogo.php?palabraClave=<?= 'Pre-entrenos' ?>'">Pre-entrenos</button>
                                <button type="button" id="btn_proteinas" class="dropdown-item"
                                    onclick="window.location.href = 'catalogo.php?palabraClave=<?= 'Multivitamínicos' ?>'">Multivitamínicos</button>
                            <?php
                                } else {
                            ?>
                                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="¡Regístrate o inicia sesión para disfrutar de todo nuestro contenido!">
                                    <li><a class="dropdown-item disabled" aria-disabled="true" href="#">Proteínas</a></li>
                                </span>
                                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="¡Regístrate o inicia sesión para disfrutar de todo nuestro contenido!">
                                    <li><a class="dropdown-item disabled" aria-disabled="true" href="#">Aminoácidos</a></li>
                                </span>
                                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="¡Regístrate o inicia sesión para disfrutar de todo nuestro contenido!">
                                    <li><a class="dropdown-item disabled" aria-disabled="true" href="#">Creatinas</a></li>
                                </span>
                                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="¡Regístrate o inicia sesión para disfrutar de todo nuestro contenido!">
                                    <li><a class="dropdown-item disabled" aria-disabled="true" href="#">Quemadores</a></li>
                                </span>
                                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="¡Regístrate o inicia sesión para disfrutar de todo nuestro contenido!">
                                    <li><a class="dropdown-item disabled" aria-disabled="true" href="#">Pre-entrenos</a></li>
                                </span>
                                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="¡Regístrate o inicia sesión para disfrutar de todo nuestro contenido!">
                                    <li><a class="dropdown-item disabled" aria-disabled="true" href="#">Multivitamínicos</a></li>
                                </span>
                            <?php
                                }
                            ?>
                        </ul>
                    </li>
                    <li class="nav-item dropdown px-2 py-2">
                        <a class="nav-link dropdown-toggle text-white" href="#" role="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            Marcas
                        </a>
                        <ul class="dropdown-menu">
                            <?php
                                if (isset($_SESSION["correo_session"])) {
                            ?>
                                <button type="button" id="btn_proteinas" class="dropdown-item"
                                    onclick="window.location.href = 'catalogo.php?palabraClave=<?= 'ProScience' ?>'">ProScience</button>
                                <button type="button" id="btn_proteinas" class="dropdown-item"
                                    onclick="window.location.href = 'catalogo.php?palabraClave=<?= 'Nutramerican' ?>'">Nutramerican</button>
                                <button type="button" id="btn_proteinas" class="dropdown-item"
                                    onclick="window.location.href = 'catalogo.php?palabraClave=<?= 'Muscletech' ?>'">Muscletech</button>
                                <button type="button" id="btn_proteinas" class="dropdown-item"
                                    onclick="window.location.href = 'catalogo.php?palabraClave=<?= 'Smart Muscle' ?>'">Smart
                                    Muscle</button>
                            <?php
                                } else {
                            ?>
                                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="¡Regístrate o inicia sesión para disfrutar de todo nuestro contenido!">
                                    <li><a class="dropdown-item disabled" aria-disabled="true" href="#">ProScience</a></li>
                                </span>
                                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="¡Regístrate o inicia sesión para disfrutar de todo nuestro contenido!">
                                    <li><a class="dropdown-item disabled" aria-disabled="true" href="#">Nutramerican</a></li>
                                </span>
                                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="¡Regístrate o inicia sesión para disfrutar de todo nuestro contenido!">
                                    <li><a class="dropdown-item disabled" aria-disabled="true" href="#">Muscletech</a></li>
                                </span>
                                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="¡Regístrate o inicia sesión para disfrutar de todo nuestro contenido!">
                                    <li><a class="dropdown-item disabled" aria-disabled="true" href="#">Smart Muscle</a></li>
                                </span>
                            <?php
                                }
                            ?>
                        </ul>
                    </li>
                    <!-- <li class="nav-item px-2 py-2">
                        <?php
                            //if (isset($_SESSION["correo_session"])) {
                        ?>
                            <a class="nav-link text-white" href="#">Comparar</a>
                        <?php
                            //} else {
                        ?>
                            <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-placement="bottom" data-bs-content="¡Regístrate o inicia sesión para disfrutar de todo nuestro contenido!">
                                <a class="nav-link text-white" href="#">Comparar</a>
                            </span>
                        <?php
                            //}
                        ?>
                    </li> -->
                </ul>
            </div>
        </div>
    </nav>
    <!-- Inicializar los popovers -->
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]');
            const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl));
        });
    </script>