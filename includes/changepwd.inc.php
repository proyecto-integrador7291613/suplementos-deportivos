<?php

session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $userEmail = $_SESSION["correo_session"];
    $contrasena_actual = htmlspecialchars($_POST["contrasena_actual"], ENT_QUOTES, 'UTF-8');
    $nueva_contrasena = htmlspecialchars($_POST["nueva_contrasena"], ENT_QUOTES, 'UTF-8');
    $confirmar_contrasena = htmlspecialchars($_POST["confirmar_contrasena"], ENT_QUOTES, 'UTF-8');
    
    include "../classes/dbh.classes.php";
    include "../classes/models/changepwd-model.classes.php";
    include "../classes/controllers/changepwd-contr.classes.php";
    $changePwd = new ChangePwdContr($userEmail, $contrasena_actual, $nueva_contrasena, $confirmar_contrasena);

    $changePwd->UpdatePwd($userEmail);

    $_SESSION['password_change_success'] = true;
    header("location: ../profile.php?error=none");
    exit();
}

