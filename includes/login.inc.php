<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") { 
  // Grabbing the data
  $correo = htmlspecialchars($_POST["correo"], ENT_QUOTES, 'UTF-8' );
  $contrasena = htmlspecialchars($_POST["contrasena"], ENT_QUOTES, 'UTF-8' );

  // Instantiate SignupContr class
  include "../classes/dbh.classes.php";
  include "../classes/models/login-model.classes.php";
  include "../classes/controllers/login-contr.classes.php";

  $login = new LoginContr($correo, $contrasena);
  // Runnint error handlers and user signup
  $login->loginUser();

  // Going back to front page
  header("location: ../index.php?error=noneFinal");
}