<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $nombres = htmlspecialchars($_POST["nombres"], ENT_QUOTES, 'UTF-8');
  $apellidos = htmlspecialchars($_POST["apellidos"], ENT_QUOTES, 'UTF-8');
  $telefono = htmlspecialchars($_POST["telefono"], ENT_QUOTES, 'UTF-8');
  $correo = htmlspecialchars($_POST["correo"], ENT_QUOTES, 'UTF-8');
  $contrasena = htmlspecialchars($_POST["contrasena"], ENT_QUOTES, 'UTF-8');
  $confirmar_contrasena = htmlspecialchars($_POST["confirmar_contrasena"], ENT_QUOTES, 'UTF-8');

  // Inclusión clases
  include "../classes/dbh.classes.php";
  include "../classes/models/signup-model.classes.php";
  include "../classes/controllers/signup-contr.classes.php";
  include "../classes/models/login-model.classes.php";
  include "../classes/controllers/login-contr.classes.php";

  $signup = new SignupContr($nombres, $apellidos, $telefono, $correo, $contrasena, $confirmar_contrasena);

  $signup->signupUser();

  $login = new LoginContr($correo, $contrasena);
  $login->loginUser();

  // Going back to front page
  header("location: ../index.php?error=none");
}