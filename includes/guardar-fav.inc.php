<?php
//  para que session_start() solo se llame si no hay una sesión activa:
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['producto']) && isset($_GET['from'])) {
    $userId = $_SESSION["id_session"];
    $productId = $_GET['producto'];
    $from = $_GET['from'];

    // Inclusión clases
    include "../classes/dbh.classes.php";
    include "../classes/models/guardar-fav-model.class.php";
    include "../classes/controllers/guardar-fav-contr.class.php";

    $favoritosController = new GuardarFavContr($userId, $productId);
    $favoritosController->gestionarFavorito();

    if ($from === 'catalogo' && isset($_GET['palabraClave'])) {
        $palabraClave = $_GET['palabraClave'];
        header("location: ../catalogo.php?palabraClave=" . urlencode($palabraClave));
    } elseif ($from === 'producto') {
        header("location: ../producto.php?producto=" . urlencode($productId));
    } elseif ($from === 'favoritos') {
        header("location: ../favoritos.php");
    } 

}

?>