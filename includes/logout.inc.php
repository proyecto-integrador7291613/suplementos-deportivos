<?php
session_start(); // Because we need to start a session in order to destroy it if there is running a session
session_unset(); // To unset all the session variables
session_destroy();

header("location: ../index.php?error=none"); 