<?php

session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $userEmail = $_SESSION["correo_session"];

    include "../classes/dbh.classes.php";
    include "../classes/models/deleteuser-model.classes.php";
    include "../classes/controllers/deleteuser-contr.classes.php";

    $deleteUser = new DeleteUserContr(); 

    $deleteUser->deleteAccount($userEmail);

    header("location: ../index.php?success=accountdeleted");

}