<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$userId = $_SESSION["id_session"];

include "./classes/dbh.classes.php";
include "./classes/models/favorito-model.classes.php";
include "./classes/views/favorito-view.classes.php";

// Crear una instancia del modelo FavoritoModel
$favoritoModel = new FavoritoModel();

// Obtener los favoritos y asignarlos a una variable
$favoritosInfo = $favoritoModel->getFavorito($userId);

// Crear una instancia de la vista FavoritoView
$favoritoView = new FavoritoView();

// Llamar al método de la vista para mostrar los resultados
$favoritoView->mostrarFavoritos($userId, $favoritosInfo);

?>