<?php

session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $userEmail = $_SESSION["correo_session"];
    $nombres = htmlspecialchars($_POST["nombres"], ENT_QUOTES, 'UTF-8');
    $apellidos = htmlspecialchars($_POST["apellidos"], ENT_QUOTES, 'UTF-8');
    $telefono = htmlspecialchars($_POST["telefono"], ENT_QUOTES, 'UTF-8');
    
    include "../classes/dbh.classes.php";
    include "../classes/models/profileinfo-model.classes.php";
    include "../classes/controllers/profileinfo-contr.classes.php";
    $profileInfo = new ProfileInfoContr($userEmail);

    //refer to the method that update the DB --> in profile-contr class
    $profileInfo->updateProfileInfo($nombres, $apellidos, $telefono);

    $_SESSION['profile_update_success'] = true;
    header("location: ../profile.php?error=none");
    exit();
}

