<?php

if (isset($_GET['palabraClave'])) {
  $palabraClave = $_GET['palabraClave'];

  include "./classes/dbh.classes.php";
  include "./classes/models/catalogo-model.classes.php";
  include "./classes/views/catalogo-view.classes.php";

  $vista = new CatalogoView($palabraClave);
}