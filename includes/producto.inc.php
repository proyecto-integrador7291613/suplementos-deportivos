<?php

if (isset($_GET['producto'])) {
  $producto = $_GET['producto'];

  include "./classes/dbh.classes.php";
  include "./classes/models/producto-model.classes.php";
  include "./classes/views/producto-view.classes.php";

  $producto = new ProductoView($producto);
}