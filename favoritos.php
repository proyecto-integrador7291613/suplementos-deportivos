<?php
include_once "header/header_index.php";
include "includes/favorito.inc.php";
include "includes/guardar-fav.inc.php";
?>

<head>
    <title>Mis Favoritos | Guía de Suplementos Deportivos</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/styles_favoritos.css">
</head>

<body>
    <section class="favoritos" style="padding-top: 104px; padding-bottom: 50px;">
        <div class="container">
            <div class="title pt-4 pb-2">
                <h2 class="position-relative d-inline-block">
                    Mis Favoritos
                </h2>
            </div>
            <?php
            $nProductos = count($favoritosInfo);
            if ($nProductos > 1) {
            ?>
                <span class="badge bg-secondary">
                    <?php echo $nProductos; ?> favoritos
                </span>
            <?php
                } elseif ($nProductos == 1) {
            ?>
                <span class="badge bg-secondary">
                    <?php echo $nProductos; ?> favorito
                </span>
            <?php
                }
            else {
            ?>
                <!-- <p>No hay favoritos</p> -->
            <?php
                }
            ?>
            <div class="row g-0">
                <?php
                if (!empty($favoritosInfo)) {
                    for ($i = 0; $i < $nProductos; $i++) {
                        $favorito = $favoritosInfo[$i];
                ?>
                        <div class="col-md-6 col-lg-4 col-xl-3 p-2">
                            <a href="producto.php?producto=<?php echo urlencode($favorito['id']); ?>">
                                <div class="card h-100 d-flex">
                                    <div class="card-img position-relative overflow-hidden h-100 d-flex flex-column justify-content-center align-items-center">
                                        <img src="<?php echo $favorito['foto']; ?>" class="card-img-top" alt="Imagen del suplemento">
                                    </div>
                                    <div class="card-body h-30 border align-items-end">
                                        <h3><?php echo $favorito['nombre']; ?></h3>
                                        <h5>
                                            <?php echo $favorito['nombre_categoria']; ?>
                                            <span>
                                                <i class="<?= $favoritoView->verificarFavorito($favorito) ? 'fa-solid' : 'fa-regular' ?> fa-heart guardar-favorito" data-producto="<?php echo $favorito['id']; ?>"></i>
                                                <!-- operadores ternarios: condición ? valor_si_verdadero : valor_si_falso-->
                                            </span>                                        
                                        </h5>
                                    </div>
                                </div>
                            </a>
                        </div>
                <?php
                    }
                ?>
                <script>
                    document.addEventListener('DOMContentLoaded', function () {
                    // Espera a que el DOM esté completamente cargado antes de ejecutar el código
                    var corazones = document.querySelectorAll('.guardar-favorito');
                    // Busca todos los elementos que tienen la clase 'guardar-favorito'
                    corazones.forEach(function (corazon) {
                        // Itera sobre cada uno de esos elementos
                        corazon.addEventListener('click', function (event) {
                            // Agrega un evento de clic a cada elemento
                            event.preventDefault(); // Evita el comportamiento predeterminado del enlace
                            var idProducto = this.getAttribute('data-producto');
                            // Obtiene el valor del atributo 'data-producto' de cada elemento, que contiene el ID del producto
                            window.location.href = 'includes/guardar-fav.inc.php?producto=' + idProducto + '&from=favoritos';
                            // Redirige al usuario a 'guardar_favorito.php' con el ID del producto como parámetro en la URL
                        });
                    });
                });
                </script> 
                <?php
                } else {
                ?>
                    <div class="no-fav text-center">
                        <div class="heart-img">
                            <img src="img/heart.png" alt="Imagen de un corazón">
                        </div>
                        <h4>¡No hay favoritos!</h4>
                        <p>Busca y agrega suplementos como favoritos para verlos aquí</p>
                    </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>

    <!-- Sección pie de página -->
    <footer>
        <div class="useful-links">
            <ul>
                <img src="img/logo_blanco.png" class="mb-3" width="100" alt="Logo de la página">
                <li><a href="index_log.php">Inicio</a></li>
                <li><a href="https://sanmonor.github.io/Proyecto_Suplementos_Deportivos/" target="_blank" rel="noopener noreferrer">Nuestro Portafolio</a></li>
                <li><a href="#">Términos y condiciones</a></li>
            </ul>
        </div>
        <div class="copyright">
            <p>&copy; 2023 Guía de suplementos deportivos. Todos los derechos reservados.</p>
        </div>
    </footer>

    <!-- JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
</body>

</html>
