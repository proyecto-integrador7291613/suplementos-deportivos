document.getElementById("formDeleteUser").addEventListener("submit", function(event) {
    event.preventDefault(); // Previene el envío del formulario inmediatamente
    (async () => {
        const {value: respuesta} = await Swal.fire({
            //title: '¡Cuidado!',
            text: '¿Estás seguro que deseas eliminar tu cuenta?',
            icon: 'warning',
            confirmButtonText: 'Aceptar',
            confirmButtonColor: 'rgb(168, 14, 14)',
            confirmButtonAriaLabel: 'Aceptar',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            cancelButtonColor: '#68a4ac',
            cancelButtonAriaLabel: 'Cancelar'
        });
        
        if (respuesta) {
            event.target.submit(); // Si el usuario confirma, se envía el formulario
        }
    })();
});

