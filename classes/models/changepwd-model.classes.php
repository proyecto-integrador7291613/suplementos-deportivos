<?php

class ChangePwdModel extends Dbh {
    protected function getActualPwd($userEmail) {
        $stmt = $this->connect()->prepare('SELECT contrasena FROM Usuarios WHERE email = ?;');

        if (!$stmt->execute(array($userEmail))) { 
            $stmt = null;
            header("location: ../profile.php?error=stmtfailed");
            exit(); 
        }

        if ($stmt->rowCount() == 0) {
            $stmt = null;
            header("location: ../profile.php?error=usernotfound");
            exit(); 
        }

        $ActualPwd = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt = null;
        return $ActualPwd;
    }

    protected function setNewProfileInfo($nombres, $apellidos, $telefono, $correo) {
        $stmt = $this->connect()->prepare('UPDATE Usuarios SET nombres = ?, apellidos = ?, telefono = ? WHERE email = ?;');

        if (!$stmt->execute(array($nombres, $apellidos, $telefono, $correo))) {
            $stmt = null;
            header("location: ../profile.php?error=stmtfailed");
            exit();
        }

        $stmt = null;
    }
    
    protected function setNewPwd($pwdNew, $userEmail) {
        $stmt = $this->connect()->prepare('UPDATE Usuarios SET contrasena = ? WHERE email = ?;');

        //make sure we has the password before inserting it
        $hashedPwd = password_hash($pwdNew, PASSWORD_DEFAULT); //method to hash any sort of password or string (kind of hashing method)

        if(!$stmt->execute(array($hashedPwd, $userEmail))) {
            $stmt = null;
            header("location: ../profile.php?error=stmtfailed");
            exit();
        }
        
        $stmt = null; //end off our statements
    }
}