<?php

class ProductoModel extends Dbh {
  protected function getProductInfo($id) {
    $stmt = $this->connect()->prepare("SELECT S.*, M.nombre AS nombre_marca, C.nombre AS nombre_categoria 
    FROM Suplementos S
    INNER JOIN Marcas M ON M.id = S.marca_id
    INNER JOIN Categorias C ON C.ID = S.categoria_id
    WHERE S.id=?;");

    $stmt->execute(array($id));

    $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $info;
  } 

  protected function getProductNutrients($id) {
    $stmt = $this->connect()->prepare("SELECT N.nombre AS nutriente_nombre, NS.nutriente_valor, NS.nutriente_unidades, NS.porcentaje_valor_diario
    FROM Suplementos S
    INNER JOIN Nutrientes_x_suplemento NS ON NS.suplemento_id = S.id
    INNER JOIN Nutrientes N ON N.id = NS.nutriente_id
    WHERE S.id=?;");

    $stmt->execute(array($id));

    $nutrientes = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $nutrientes;
  } 
  
  protected function esFavorito($userId, $productId) {
    $stmt = $this->connect()->prepare("SELECT * FROM Favoritos WHERE usuario_id = ? AND suplemento_id = ?");
    $stmt->execute([$userId, $productId]);
    return $stmt->fetch() ? true : false;
  }
}
