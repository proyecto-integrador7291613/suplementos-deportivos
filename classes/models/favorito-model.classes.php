<?php

class FavoritoModel extends Dbh {
    public function getFavorito($userId) {
        // Consulta SQL para obtener los favoritos y sus detalles
        $sql = "SELECT S.*,S.foto, C.nombre AS nombre_categoria, M.nombre AS nombre_marca
                FROM Favoritos F
                INNER JOIN Suplementos S ON F.suplemento_id = S.id
                INNER JOIN Categorias C ON C.id = S.categoria_id
                INNER JOIN Marcas M ON M.id = S.marca_id
                WHERE usuario_id = '$userId'
                ORDER BY S.nombre ASC";
        
        // Ejecutar la consulta
        $result = $this->connect()->query($sql);
        
        // Inicializar un array para almacenar los resultados
        $resultados = [];
        
        // Verificar si hay resultados
        if ($result->rowCount() > 0) {
            // Iterar sobre los resultados y almacenarlos en el array
            while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                $resultados[] = $row;
            }
        } 
        // Devolver el array de resultados
        return $resultados;
    }

    protected function esFavorito($userId, $productId) {
        $stmt = $this->connect()->prepare("SELECT * FROM Favoritos WHERE usuario_id = ? AND suplemento_id = ?");
        $stmt->execute([$userId, $productId]);
        return $stmt->fetch() ? true : false;
    }
}