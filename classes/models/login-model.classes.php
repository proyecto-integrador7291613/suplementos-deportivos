<?php

class LoginModel extends Dbh {
  protected function getUser($correo, $contrasena) {
    $stmt = $this->connect()->prepare('SELECT contrasena FROM Usuarios WHERE email = ?');

    if (!$stmt->execute(array($correo))) { // Possible error? or modification needed
      $stmt = null;
      header("location: ../login.php?error=stmtfailed1");
      exit();
    }

    if ($stmt->rowCount() == 0) { // Possibly modification needed
      $stmt = null;
      header("location: ../login.php?error=userNotFound1");
      exit();
    }

    $pwdHashed = $stmt->fetchAll(pdo::FETCH_ASSOC);

    $checkPwd = password_verify($contrasena, $pwdHashed[0]["contrasena"]);

    if ($checkPwd == false) { // Possibly modification needed
      $stmt = null;
      header("location: ../login.php?error=passwordUnmatched");
      exit();
    } 
    else if ($checkPwd == true) {
      $stmt = $this->connect()->prepare('SELECT * FROM Usuarios WHERE email = ? AND contrasena = ?');

      if (!$stmt->execute(array($correo, $pwdHashed[0]["contrasena"]))) {
        $stmt = null;
        header("location: ../login.php?error=stmtFailed2");
        exit();
      }

      if ($stmt->rowCount() == 0) { // Possibly modification needed
        $stmt = null;
        header("location: ../login.php?error=userNotFound2");
        exit();
      }

      $usuario_info = $stmt->fetchAll(pdo::FETCH_ASSOC);

      session_start();
      $_SESSION["correo_session"] = $usuario_info[0]["email"];
      $_SESSION["contrasena_session"] = $usuario_info[0]["contrasena"];
      $_SESSION["nombre_session"] = $usuario_info[0]["nombres"];
      $_SESSION["id_session"] = $usuario_info[0]["id"];

      $stmt = null;
    }
    $stmt = null;
  }
}