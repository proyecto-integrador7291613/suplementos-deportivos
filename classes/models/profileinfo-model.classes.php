<?php

class ProfileInfoModel extends Dbh {
    protected function getProfileInfo($userEmail) {
        $stmt = $this->connect()->prepare('SELECT * FROM Usuarios WHERE email = ?;');

        if (!$stmt->execute(array($userEmail))) { 
            $stmt = null;
            header("location: profile.php?error=stmtfailed");
            exit(); 
        }

        if ($stmt->rowCount() == 0) {
            $stmt = null;
            header("location: profile.php?error=profilenotfound");
            exit(); 
        }

        $profileData = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $profileData;
    }

    protected function setNewProfileInfo($nombres, $apellidos, $telefono, $correo) {
        $stmt = $this->connect()->prepare('UPDATE Usuarios SET nombres = ?, apellidos = ?, telefono = ? WHERE email = ?;');

        if (!$stmt->execute(array($nombres, $apellidos, $telefono, $correo))) {
            $stmt = null;
            header("location: profile.php?error=stmtfailed");
            exit();
        }
        
        $_SESSION["nombre_session"] = $nombres;

        $stmt = null;
    }
    
    protected function validatePwd($correo, $contrasena) {
        $result = false;
        $stmt = $this->connect()->prepare('SELECT contrasena FROM Usuarios WHERE email = ?;');

        if (!$stmt->execute(array($correo))) { // Possible error? or modification needed
            $stmt = null;
            header("location: ../profile.php?error=stmtfailed1");
            exit();
        }

        $pwdHashed = $stmt->fetchAll(pdo::FETCH_ASSOC);

        $checkPwd = password_verify($contrasena, $pwdHashed[0]["contrasena"]);

        if ($checkPwd == false) { // Possibly modification needed
            $stmt = null;
            header("location: ../profile.php?error=passwordUnmatched");
            exit();
        } 
        elseif ($checkPwd == true) {
            $result = true;
        }
    }
}