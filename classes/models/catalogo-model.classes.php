<?php

class CatalogoModel extends Dbh {
  protected function getProducts($palabraClave) {
    $stmt = $this->connect()->prepare("SELECT S.*, C.nombre AS nombre_categoria, M.nombre AS nombre_marca
      FROM Categorias C
      INNER JOIN Suplementos S ON C.id = S.categoria_id
      INNER JOIN Marcas M ON M.id = S.marca_id
      WHERE C.nombre LIKE :palabraClave OR M.nombre LIKE :palabraClave OR S.nombre LIKE :palabraClave
      ORDER BY S.nombre ASC;");

    $parametro = '%' . $palabraClave . '%';
    $stmt->bindParam(':palabraClave', $parametro, PDO::PARAM_STR);

    if (!$stmt->execute()) {
        $stmt = null;
        header("location: index.php?error=stmtfailed");
        exit(); 
    }

    if ($stmt->rowCount() == 0) {
        $stmt = null;
        header("location: index.php?error=productNotFound");
        exit();
    }

    $producto = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $producto;
  } 

  protected function esFavorito($userId, $productId) {
    $stmt = $this->connect()->prepare("SELECT * FROM Favoritos WHERE usuario_id = ? AND suplemento_id = ?");
    $stmt->execute([$userId, $productId]);
    return $stmt->fetch() ? true : false;
  }
}
