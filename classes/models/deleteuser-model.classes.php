<?php

class DeleteUserModel extends Dbh {
    public function deleteFavorites($userEmail) {
        $stmt = $this->connect()->prepare('DELETE FROM Favoritos WHERE usuario_id = (SELECT id FROM Usuarios WHERE email = ?);');

        if (!$stmt->execute(array($userEmail))) { 
            $stmt = null;
            header("location: ../profile.php?error=stmtfailed");
            exit(); 
        }

        return true; 

    }

    public function deleteUser($userEmail) {
        $stmt = $this->connect()->prepare('DELETE FROM Usuarios WHERE email = ?;');

        if (!$stmt->execute(array($userEmail))) { 
            $stmt = null;
            header("location: ../profile.php?error=stmtfailed");
            exit(); 
        }

        return $stmt->rowCount() > 0; //retorna verdadero si se eliminó 

    }
}