<?php

class SignupModel extends Dbh {
    protected function setUser($nombres, $apellidos, $telefono, $correo, $contrasena) {
        $stmt = $this->connect()->prepare('INSERT INTO Usuarios (nombres, apellidos, telefono, email, contrasena) VALUES (?,?,?,?,?);');

        $hashedpwd = password_hash($contrasena, PASSWORD_DEFAULT);

        if (!$stmt->execute(array($nombres, $apellidos, $telefono, $correo, $hashedpwd))) {
            $stmt = null;
            header("location: ../registro.php?error=stmtfailed");
            exit();
        }

        $stmt = null;
    }

    protected function checkEmail($correo){
        $stmt = $this->connect()->prepare('SELECT id FROM Usuarios WHERE email = ?;');

        if (!$stmt->execute(array($correo))) {
            $stmt = null;
            header("location: ../registro.php?error=stmtfailed");
            exit();
        }

        $resultCheck = null;
        if ($stmt->rowCount() > 0) {
            $resultCheck = false;
        }
        else {
            $resultCheck = true;
        }

        return $resultCheck;
    }
}