<?php

class GuardarFavModel extends Dbh{
    public function esFavorito($userId, $productId) {
        $stmt = $this->connect()->prepare("SELECT * FROM Favoritos WHERE usuario_id = ? AND suplemento_id = ?");
        $stmt->execute([$userId, $productId]);
        return $stmt->fetch() ? true : false;
    }

    protected function setFavorito($userId, $productId) {
        $stmt = $this->connect()->prepare('INSERT INTO Favoritos (usuario_id, suplemento_id) VALUES (?,?);');

        if (!$stmt->execute(array($userId, $productId))) {
            $stmt = null;
            header("location: ../catalogo.php?error=stmtfailed");
            exit();
        }

        $stmt = null;
    }

    protected function removeFavorito($userId, $productId) {
        $stmt = $this->connect()->prepare("DELETE FROM Favoritos WHERE usuario_id = ? AND suplemento_id = ?;");
        
        if (!$stmt->execute(array($userId, $productId))) {
            $stmt = null;
            header("location: ../catalogo.php?error=stmtfailed");
            exit();
        }

        $stmt = null;
    }
}
?>
