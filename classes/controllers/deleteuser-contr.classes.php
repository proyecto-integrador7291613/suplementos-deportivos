<?php

class DeleteUserContr extends DeleteUserModel {
    public function deleteAccount($userEmail) {
        $favoritesDeleted = $this->deleteFavorites($userEmail);
        if (!$favoritesDeleted) {
            header("location: ../profile.php?error=deletefavoritesfailed");
            exit();
        }

        $accountDeleted = $this->deleteUser($userEmail);
        if (!$accountDeleted) {
            header("location: ../profile.php?error=deleteuserfailed");
            exit();
        }
        
        session_unset();
        session_destroy();
    }
}