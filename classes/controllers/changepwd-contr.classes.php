<?php

class ChangePwdContr extends ChangePwdModel {
    private $userEmail;
    private $pwd;
    private $pwdNew;
    private $pwdRepeat;

    public function __construct($userEmail, $pwd, $pwdNew, $pwdRepeat) {
        $this->userEmail = $userEmail;
        $this->pwd = $pwd;
        $this->pwdNew = $pwdNew;
        $this->pwdRepeat = $pwdRepeat;
    }

    public function UpdatePwd() {
        if ($this->validatePwd($this->userEmail) == false) {
            //echo "Actual Password incorrect!";
            header("location: ../profile.php?error=actualpasswordincorrect");
            exit();
        }
        if ($this->pwdMatch() == false) {
            //echo "New Passwords don't match!";
            header("location: ../profile.php?error=newpasswordsunmatched");
            exit();
        }

        $this->setNewPwd($this->pwdNew, $this->userEmail);
    }

    private function validatePwd($userEmail) {
        $result = null;
        $actualPwd = $this->getActualPwd($userEmail);

        $checkPwd = password_verify($this->pwd, $actualPwd[0]["contrasena"]);

        if ($checkPwd == false) { // Possibly modification needed
            $result = false;
        } 
        elseif ($checkPwd == true) {
            $result = true;
        }
        return $result;
    }

    private function pwdMatch() {
        $result = null;
        if ($this->pwdNew !== $this->pwdRepeat) //if pwd isn't the same as pwdRepeat
        {
            $result = false;
        }
        else {
            $result = true;
        }
        return $result;
    }
}