<?php

class GuardarFavContr extends GuardarFavModel {
    //Properties of the class
    private $userId;
    private $productId;

    //Constructor
    public function __construct($userId, $productId) {
        $this->userId = $userId;
        $this->productId = $productId;
    }

    public function gestionarFavorito() {
        $favoritosModel = new GuardarFavModel();

        if ($favoritosModel->esFavorito($this->userId, $this->productId)) {
            $favoritosModel->removeFavorito($this->userId, $this->productId);
        } else {
            $favoritosModel->setFavorito($this->userId, $this->productId);
        }
    }

}