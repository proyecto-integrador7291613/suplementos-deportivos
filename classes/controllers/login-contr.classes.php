<?php

class LoginContr extends LoginModel {
  private $correo;
  private $contrasena;

  public function __construct($correo, $contrasena)
  {
    $this->correo = $correo;
    $this->contrasena = $contrasena;
  }
  public function loginUser() {
    if ($this->emptyInput() == true) {
      header("location: ../login.php?error=emptyinput!"); 
      exit();
    }

    $this->getUser($this->correo, $this->contrasena);
  }

  private function emptyInput() {
    if (empty($this->correo) || empty($this->contrasena)) {
      $result = true;
    } else {
      $result = false;
    }
  }
}