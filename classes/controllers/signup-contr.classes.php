<?php

class SignupContr extends SignupModel {
    //Properties of the class
    private $nombres;
    private $apellidos;
    private $telefono;
    private $correo;
    private $contrasena;
    private $confirmar_contrasena;

    //Constructor
    public function __construct($nombres, $apellidos, $telefono, $correo, $contrasena, $confirmar_contrasena) {
        $this->nombres = $nombres;
        $this->apellidos = $apellidos;
        $this->telefono = $telefono;
        $this->correo = $correo;
        $this->contrasena = $contrasena;
        $this->confirmar_contrasena = $confirmar_contrasena;
    }

    public function signupUser() {
        if ($this->emptyInput() == false) {
            //echo "Empty input!";
            header("location: ../registro.php?error=emptyinput");
            exit();
        }
        if ($this->invalidNameLastname() == false) {
            //echo "Invalid username!";
            header("location: ../registro.php?error=nameorlastname");
            exit();
        }
        if ($this->invalidPhone() == false) {
            //echo "Invalid email!";
            header("location: ../registro.php?error=phone");
            exit();
        }
        if ($this->invalidEmail() == false) {
            //echo "Invalid email!";
            header("location: ../registro.php?error=email");
            exit();
        }
        if ($this->invalidPwdLenght() == false) {
            //echo "Invalid email!";
            header("location: ../registro.php?error=passwordlenght");
            exit();
        }
        if ($this->pwdMatch() == false) {
            //echo "Passwords don't match!";
            header("location: ../registro.php?error=passwordmatch");
            exit();
        }
        if ($this->emailTakenCheck() == false) {
            //echo "Username or email taken!";
            header("location: ../registro.php?error=emailtaken");
            exit();
        }

        $this->setUser($this->nombres, $this->apellidos, $this->telefono, $this->correo, $this->contrasena);
    }

    //Error Handlers
    private function emptyInput() {
        $result = null;
        if (empty($this->nombres) || empty($this->apellidos) || empty($this->telefono) || empty($this->correo) || empty($this->contrasena) || empty($this->confirmar_contrasena)) {
            $result = false;
        }
        else {
            $result = true;
        }
        return $result;
    }

    private function invalidNameLastname() {
        $result = null;
        if (!preg_match("/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s'-]*$/", $this->nombres) || !preg_match("/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s'-]*$/", $this->apellidos)) {
            $result = false;
        }
        else {
            $result = true;
        }
        return $result;
    }

    private function invalidPhone() {
        $result = null;
        if (!preg_match("/^[0-9]{10}$/", $this->telefono)) {
            $result = false;
        }
        else {
            $result = true;
        }
        return $result;
    }
    
    private function invalidEmail() {
        $result = null;
        if (!filter_var($this->correo, FILTER_VALIDATE_EMAIL)) {
            $result = false;
        }
        else {
            $result = true;
        }
        return $result;
    }

    private function invalidPwdLenght() {
        $result = null;
        if (strlen($this->contrasena) < 8 || strlen($this->confirmar_contrasena) < 8) {
            $result = false;
        }
        else {
            $result = true;
        }
        return $result;
    }

    private function pwdMatch() {
        $result = null;
        if ($this->contrasena !== $this->confirmar_contrasena) {
            $result = false;
        }
        else {
            $result = true;
        }
        return $result;
    }

    private function emailTakenCheck() {
        $result = null;
        if (!$this->checkEmail($this->correo)) {
            $result = false;
        }
        else {
            $result = true;
        }
        return $result;
    }

    
}