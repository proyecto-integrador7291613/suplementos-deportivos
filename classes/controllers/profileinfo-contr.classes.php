<?php

class ProfileInfoContr extends ProfileInfoModel {
    private $userEmail;

    public function __construct($userEmail) {
        $this->userEmail = $userEmail;
    }
    public function updateProfileInfo($nombres, $apellidos, $telefono) {
        //error handlers
        if ($this->emptyInputCheck($nombres, $apellidos, $telefono) == true) {
            header("location: ../profilesettings.php?error=emptyinput");
            exit();
        }
        if ($this->invalidNameLastname($nombres, $apellidos) == false) {
            //echo "Invalid username!";
            header("location: ../profilesettings.php?error=nameorlastname");
            exit();
        }
        if ($this->invalidPhone($telefono) == false) {
            //echo "Invalid email!";
            header("location: ../profilesettings.php?error=phone");
            exit();
        }
        //update profile info
        $this->setNewProfileInfo($nombres, $apellidos, $telefono, $this->userEmail);
    }

    private function emptyInputCheck($nombres, $apellidos, $telefono) {
        $result = null;
        if (empty($nombres) || empty($apellidos) || empty($telefono)) {
            $result = true;
        }
        else {
            $result = false;
        }
        return $result;
    }

    private function invalidNameLastname($nombres, $apellidos) {
        $result = null;
        if (!preg_match("/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s'-]*$/", $nombres) || !preg_match("/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s'-]*$/", $apellidos)) {
            $result = false;
        }
        else {
            $result = true;
        }
        return $result;
    }

    private function invalidPhone($telefono) {
        $result = null;
        if (!preg_match("/^[0-9]{10}$/", $telefono)) {
            $result = false;
        }
        else {
            $result = true;
        }
        return $result;
    }
}