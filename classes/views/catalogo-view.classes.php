<?php

class CatalogoView extends CatalogoModel { 
  private $productos = null;

  public function __construct($palabraClave)
  {
    $this->productos = $this->getProducts($palabraClave);
  }

  public function fetchProductsNumber() {
    return count($this->productos);
  }

  public function fetchProductName($n) {
    echo $this->productos[$n]["nombre"];
  }

  public function fetchProductCategory($n) {
    echo $this->productos[$n]["nombre_categoria"];
  }

  public function fetchProductMarca($n) {
    echo $this->productos[$n]["nombre_marca"];
  }

  public function fetchProductFoto($n) {
    echo $this->productos[$n]["foto"];
  } 

  public function fetchProductId($n) {
    return $this->productos[$n]["id"];
  }

  public function verificarFavorito($n) {
    $userId = $_SESSION["id_session"];
    $productId = $this->productos[$n]["id"];
    $result = $this->esFavorito($userId, $productId);
    return $result;
  }
}