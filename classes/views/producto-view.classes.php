<?php

class ProductoView extends ProductoModel { 
  private $info = null;
  private $nutrientes = null;

  public function __construct($id)
  {
    $this->info = $this->getProductInfo($id);
    $this->nutrientes = $this->getProductNutrients($id);
  }

  public function printProductName() {
    echo $this->info[0]["nombre"];
  }

  public function printProductCategory() {
    echo $this->info[0]["nombre_categoria"];
  }

  public function printProductBrand() {
    echo $this->info[0]["nombre_marca"];
  }

  public function printProductFoto() {
    echo $this->info[0]["foto"];
  } 

  public function printProductDescription() {
    echo $this->info[0]["descripcion"];
  }

  public function printProductIngredients() {
    echo $this->info[0]["ingredientes"];
  }

  public function printProductUsage() {
    echo $this->info[0]["modo_empleo"];
  }

  public function printProductGWeight() {
    echo $this->info[0]["peso_g"];
  }

  public function printProductLbWeight() {
    echo $this->info[0]["peso_lb"];
  }

  public function fetchProductServingSize() {
    return $this->info[0]["tamano_x_porcion"];
  }

  public function fetchProductServingMeasure() {
    return $this->info[0]["medida_x_porcion"];
  }

  public function fetchProductMeasureType() {
    return $this->info[0]["tipo_medida"];
  }

  public function printProductServingsPerContainer() {
    echo $this->info[0]["porciones_x_envase"];
  }

  public function fetchProductNutrients() {
    return $this->nutrientes;
  }

  public function fetchProductsNutrientCount() {
    return count($this->nutrientes);
  }

  public function printNutrientName($i) {
    echo $this->nutrientes[$i]["nutriente_nombre"];
  }

  public function printNutrientValue($i) {
    echo $this->nutrientes[$i]["nutriente_valor"];
  }

  public function printNutrientUnit($i) {
    echo $this->nutrientes[$i]["nutriente_unidades"];
  }

  public function printNutrientPercentage($i) {
    echo $this->nutrientes[$i]["porcentaje_valor_diario"];
  }

  public function cambiarValores($opcion) {
    if ($opcion == 1) {
      foreach ($this->nutrientes["nutriente_valor"] as $value) {
        $value /= 2;
      }
    } elseif ($opcion == 2) {
      foreach ($this->nutrientes["nutriente_valor"] as $value) {
        $value *= 2;
      }
    }
  }

  public function fetchProductId() {
    echo $this->info[0]["id"];
  }

  public function verificarFavorito() {
    $userId = $_SESSION["id_session"];
    $productId = $this->info[0]["id"];
    $result = $this->esFavorito($userId, $productId);
    return $result;
  }
  
  public function fetchProductNoTabletas() {
    return $this->info[0]["n_tabletas"];
  }
}