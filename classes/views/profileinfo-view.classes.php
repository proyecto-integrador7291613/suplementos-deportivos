<?php

class ProfileInfoView extends ProfileInfoModel {
    public function fetchName($userEmail) {
        $profileInfo = $this->getProfileInfo($userEmail);

        echo $profileInfo[0]["nombres"];
    }

    public function fetchLastname($userEmail) {
        $profileInfo = $this->getProfileInfo($userEmail);

        echo $profileInfo[0]["apellidos"];
    }

    public function fetchPhone($userEmail) {
        $profileInfo = $this->getProfileInfo($userEmail);

        echo $profileInfo[0]["telefono"];
    }
}