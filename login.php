<?php
    include_once "header/header_index.php";
?>

<head>
    <title>Iniciar sesión | Guía de Suplementos Deportivos</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/styles_login.css">
</head>

    <section class="formulario seccion-gris">
        <div class="container">
            <div class="row">
                <div class=" col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                    <form  action="./includes/login.inc.php" method="POST" id="formulario" autocomplete="off">
                        <h2>¡Bienvenido!</h2>
                        <p>Que bueno tenerte de vuelta</p><br>
                        <?php
                        ?>

                        <div class="form-group">
                            <label class="label" for="correo"><strong>Correo:</strong></label>
                            <input type="email" class="form-control" id="correo" name="correo"
                                placeholder="Ingresa tu correo" required>
                        </div>

                        <div class="form-group">
                            <label class="label" for="contrasena"><strong>Contraseña:</strong></label>
                            <input type="password" class="form-control" id="contrasena" name="contrasena"
                                placeholder="Ingresa tu contraseña" required>
                        </div>

                        <div class="form-group form_action--button">
                            <input class="btn btn-primary" type="submit" value="Ingresar" id="btn_ingresar"
                                name="btn_ingresar">
                        </div>
                    </form>
                    <?php
                        if (!isset($_GET['error'])) {
                            
                        }
                        else {
                            $errorCheck = $_GET['error'];
                            
                            if ($errorCheck == "emptyinput!") {
                                echo "<p class='error'>¡Ups! Parece que te has olvidado de rellenar algún campo.</p>";
                            }
                            elseif ($errorCheck == "userNotFound1" || $errorCheck == "userNotFound2") {
                                echo "<p class='error'>¡Oh no! Parece que el usuario que ingresaste no existe en nuestro sistema.</p>";
                            }
                            elseif ($errorCheck == "passwordUnmatched") {
                                echo "<p class='error'>¡Oops! Parece que la contraseña que ingresaste no es la correcta.</p>";
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>

    <!-- Sección pie de pagina-->
    <footer>
        <div class="useful-links">
            <ul>
                <img src="img/logo_blanco.png" class="mb-3" width="100" alt="Logo de la pagina">
                <li><a href="index.php">Inicio</a></li>
                <li><a href="https://sanmonor.github.io/Proyecto_Suplementos_Deportivos/" target="_blank" rel="noopener noreferrer">Nuestro Portafolio</a></li>
                <li><a href="#">Términos y condiciones</a></li>
            </ul>
        </div>
        <div class="copyright">
            <p>&copy; 2023 Guía de suplementos deportivos. Todos los derechos reservados.</p>
        </div>
    </footer>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>
</body>

</html>