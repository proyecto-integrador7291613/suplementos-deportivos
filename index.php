<?php
    include_once "header/header_index.php";
?>

<head>
    <title>Guía de Suplementos Deportivos</title>
</head>

<!-- Seccion principal - Hero -->
<?php
    if (isset($_SESSION["correo_session"])) {
?>
    <section id="hero" class="hero vh-100" style="padding-top: 104px;">
        <div class="container h-100 d-flex align-items-center justify-content-center">
            <div class="text-center">
                <h2 class="text-white">¡Hola
                    <?php echo $_SESSION["nombre_session"]; ?>!
                </h2>
                <h1 class="py-2 fw-bold ">Guía de Suplementos Deportivos</h1>
            </div>
        </div>
    </section>
<?php
    } else {
?>
    <section id="header" class="vh-100 carousel slide" data-bs-ride="carousel" style="padding-top: 104px;">
        <div class="container h-100 d-flex align-items-center carousel-inner">
            <div class="text-center carousel-item active">
                <h2 class="text-white">¡Bienvenido!</h2>
                <h1 class="py-2 fw-bold ">Guía de Suplementos Deportivos</h1>
                <p class="text-white">Regístrate para disfrutar de todo nuestro contenido.</p>
                <a href="registro.php" class="btn mt-3">Registrarte</a>
            </div>
            <div class="text-center carousel-item">
                <h2 class="text-white">¡Bienvenido!</h2>
                <h1 class="py-2 fw-bold ">Guía de Suplementos Deportivos</h1>
                <p class="text-white">¿Ya tienes cuenta? Inicia sesión aquí</p>
                <a href="login.php" class="btn mt-3">Iniciar sesión</a>
            </div>
        </div>

        <button class="carousel-control-prev" type="button" data-bs-target="#header" data-bs-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#header" data-bs-slide="next">
            <span class="carousel-control-next-icon"></span>
        </button>
    </section>
<?php
    }
    if (isset($_SESSION["correo_session"])) {
    } else {
?>
    <!-- Sección caracteristicas-->
    <section id="caracteristicas" class="caracteristicas">
        <div class="container">
            <div class="title text-center">
                <h2 class="position-relative d-inline-block">Nuestra página</h2>
            </div>

            <div class="row g-3">
                <div class="card border-0 col-md-6 col-lg-4 bg-transparent my-3">
                    <div class="card-img position-relative overflow-hidden h-100 d-flex flex-column justify-content-center align-items-center">
                        <img src="img/suplementos2.png" width="400" alt="">
                    </div>
                    <div class="card-body px-0">
                        <h4 class="card-title text-center">Amplio catálogo informativo de suplementos deportivos</h4>
                        <p class="card-text mt-3 text-muted">Encuentra información detallada sobre diferentes tipos de
                            suplementos, sus características y cómo se pueden utilizar de manera efectiva.</p>
                    </div>
                </div>

                <div class="card border-0 col-md-6 col-lg-4 bg-transparent my-3">
                    <div class="card-img position-relative overflow-hidden h-100 d-flex flex-column justify-content-center align-items-center">
                        <img src="img/suplementos1.png" class="suplem-img" width="350" alt="">
                    </div>
                    <div class="card-body px-0">
                        <h4 class="card-title text-center">Múltiples categorías y marcas reconocidas en el mercado</h4>
                        <p class="card-text mt-3 text-muted">Encuentra una amplia gama de opciones, desde las marcas más
                            renombradas hasta aquellas más asequibles.</p>
                    </div>
                </div>

                <div class="card border-0 col-md-6 col-lg-4 bg-transparent my-3">
                    <div class="card-img position-relative overflow-hidden h-100 d-flex flex-column justify-content-center align-items-center">
                        <img src="img/comparar.png" width="300" class="" alt="Imagen referente a comparar">
                    </div>
                    <div class="card-body px-0">
                        <h4 class="card-title text-center">Comparador de productos diseñado para simplificar tu búsqueda y
                            toma de decisiones</h4>
                        <p class="card-text mt-3 text-muted">Encuentra las herramientas necesarias para comparar
                            productos en función de tus objetivos, preferencias y
                            necesidades nutricionales.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Contacto -->
    <section class="contacto">
        <div class="container">
            <div id="contacto" class="container rectangulo text-center d-flex justify-content-evenly">
                <div class="row">
                    <div class="col-12 col-md-9 col-lg-9 descripcion">
                    Regístrate para disfrutar de estos beneficios y muchos más.<br>
                    Únete a nuestra comunidad de entusiastas del fitness y la nutrición, y descubre un mundo de oportunidades para
                    optimizar tu rendimiento y mejorar tu bienestar
                    </div>
                    <div class="col-12 col-md-3 col-lg-3">
                        <a href="registro.php">
                            <button type="button">
                                Regístrate
                                <i class="fa-solid fa-arrow-right-to-bracket"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div> 
    </section>
<?php
    }
?>

<!-- Sección pie de pagina-->
<footer>
    <div class="container">
        <div class="useful-links">
            <ul>
                <img src="img/logo_blanco.png" class="mb-3" width="100" alt="Logo de la pagina">
                <li><a href="index.php">Inicio</a></li>
                <li><a href="https://sanmonor.github.io/Proyecto_Suplementos_Deportivos/" target="_blank"
                rel="noopener noreferrer">Nuestro Portafolio</a></li>
                <li><a href="#">Términos y condiciones</a></li>
            </ul>
        </div>
        <div class="copyright">
            <p>&copy; 2023 Guía de suplementos deportivos. Todos los derechos reservados.</p>
        </div>
    </div>
</footer>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
    crossorigin="anonymous"></script>
</body>

</html>