<?php
    include_once "header/header_index.php";
    include "classes/dbh.classes.php";
    include "classes/models/profileinfo-model.classes.php";
    include "classes/views/profileinfo-view.classes.php";
    $profileInfo = new ProfileInfoView();

    $showPwdAlert = isset($_SESSION['password_change_success']);
    unset($_SESSION['password_change_success']);

    $showProfileAlert = isset($_SESSION['profile_update_success']);
    unset($_SESSION['profile_update_success']);
?>
<head>
    <title>Mi cuenta | Guía de Suplementos Deportivos</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/styles_profile.css">
</head>

    <section class="cuenta seccion-blanca">
        <div class="container light-style flex-grow-1 container-p-y">
            <h2 class="font-weight-bold mb-3">
                Mi cuenta
            </h2>
            <div class="card overflow-hidden">
                <div class="row no-gutters row-bordered row-border-light">
                    <div class="col-md-3 pt-0">
                        <div class="list-group list-group-flush account-settings-links">
                            <div class="user card-body media align-items-center text">
                                <i class="fa-regular fa-user"></i>
                            </div>
                            <a class="list-group-item list-group-item-action active" data-toggle="list"
                                href="#account-general">Mi Perfil</a>
                            <a class="list-group-item list-group-item-action" data-toggle="list"
                                href="#account-change-password">Cambiar contraseña</a>
                            <a class="list-group-item list-group-item-action" data-toggle="list"
                                href="#account-notifications">Configuración avanzada</a>
                            <a class="list-group-item list-group-item-action" href="includes/logout.inc.php">Cerrar sesión</a>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div class="tab-pane fade active show" id="account-general">
                                <hr class="border-light m-0">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label class="form-label">Nombres</label>
                                        <input readonly type="text" class="form-control mb-1"
                                            value="<?php $profileInfo->fetchName($_SESSION["correo_session"]); ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Apellidos</label>
                                        <input readonly type="text" class="form-control"
                                            value="<?php $profileInfo->fetchLastname($_SESSION["correo_session"]); ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Teléfono</label>
                                        <input readonly type="text" class="form-control"
                                            value="<?php $profileInfo->fetchPhone($_SESSION["correo_session"]); ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Correo</label>
                                        <input readonly type="text" class="form-control mb-1"
                                            value="<?php echo ($_SESSION["correo_session"]); ?>">
                                        <div class="alert alert-warning mt-3">
                                            Tu correo electrónico no está confirmado. Por favor, comprueba tu bandeja de
                                            entrada.<br>
                                            <a href="javascript:void(0)">Reenviar confirmación</a>
                                        </div>
                                    </div>
                                    <div class="btn">
                                        <button type="button" id="btn_actualizar" class="btn-actualizar text-white"
                                            onclick="window.location.href = 'profilesettings.php'">Actualizar</button>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="account-change-password">
                                <div class="form-newpwd card-body pb-2">
                                    <form action="includes/changepwd.inc.php" method="POST" id="formulario" autocomplete="off">
                                        <div class="form-group">
                                            <label class="form-label">Contraseña actual</label>
                                            <input type="password" id="contrasena_actual" name="contrasena_actual"
                                                class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Nueva contraseña</label>
                                            <input type="password" id="nueva_contrasena" name="nueva_contrasena"
                                                class="form-control" required minlength="8">
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Confirmar nueva contraseña</label>
                                            <input type="password" id="confirmar_contrasena" name="confirmar_contrasena"
                                                class="form-control" required minlength="8">
                                        </div>
                                        <div class="btn">
                                            <input class="btn-actualizar text-white" type="submit" value="Guardar"
                                                id="btn_guardar_contraseña" name="btn_guardar_contraseña">
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="account-notifications">
                                <div class="card-body pb-3">
                                    <h6 class="mb-3">Notificaciones</h6>
                                    <div class="form-group">
                                        <label class="switcher">
                                            <input type="checkbox" class="switcher-input" checked>
                                            <span class="switcher-indicator">
                                                <span class="switcher-yes"></span>
                                                <span class="switcher-no"></span>
                                            </span>
                                            <span class="switcher-label">Permitir notificaciones por correo</span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="switcher">
                                            <input type="checkbox" class="switcher-input" checked>
                                            <span class="switcher-indicator">
                                                <span class="switcher-yes"></span>
                                                <span class="switcher-no"></span>
                                            </span>
                                            <span class="switcher-label">Permitir notificaciones por mensaje</span>
                                        </label>
                                    </div>
                                </div>
                                <hr class="border-light m-0">
                                <div class="card-body pt-2">
                                    <h6 class="">Eliminar cuenta</h6>
                                    <div class="form-group">
                                        <p>Lamentamos que quieras eliminar tu cuenta.</p>
                                        <div class="btn-eliminar-cuenta">
                                            <form action="includes/deleteuser.inc.php" method="POST" id="formDeleteUser" autocomplete="off">
                                                <div class="btn">
                                                    <input class="btn-borrar text-white list-group-item list-group-item-action" type="submit" value="Eliminar cuenta" id="btn_ingresar" name="btn_borrar">
                                                </div>
                                            </form>
                                            <script>
                                                /*document.getElementById("formDeleteUser").addEventListener("submit", function(event) {
                                                    var respuesta = confirm("¿Estás seguro que deseas eliminar tu cuenta?");
                                                    if (!respuesta) {
                                                        event.preventDefault(); // Evita que el formulario se envíe si el usuario cancela
                                                    }
                                                });*/
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                if (!isset($_GET['error'])) {
                    
                }
                else {
                    $errorCheck = $_GET['error'];
                    
                    if ($errorCheck == "actualpasswordincorrect") {
                        echo "<p class='error'>¡Vaya! Parece que la contraseña actual que ingresaste no es correcta.</p>";
                    }
                    elseif ($errorCheck == "newpasswordsunmatched") {
                        echo "<p class='error'>¡Ups! Parece que las nuevas contraseñas que ingresaste no coinciden.</p>";
                    }
                    elseif ($errorCheck == "usernotfound") {
                        echo "<p class='error'>Oh no! Parece que estamos teniendo problemas para encontrar tu usuario.</p>";
                    }
                }
            ?>
        </div>
    </section>

    <script>
        <?php if ($showPwdAlert): ?>
        Swal.fire({
            title: '¡Éxito!',
            icon: 'success',
            text: 'Contraseña actualizada correctamente.',
            showConfirmButton: false,
            timer: 1500
        });
        <?php endif; ?>
        
        <?php if ($showProfileAlert): ?>
        Swal.fire({
            title: '¡Éxito!',
            icon: 'success',
            text: 'Perfil actualizado correctamente.',
            showConfirmButton: false,
            timer: 1500
        });
        <?php endif; ?>
    </script>

    <!-- Sección pie de pagina-->
    <footer>
        <div class="useful-links">
            <ul>
                <img src="img/logo_blanco.png" class="mb-3" width="100" alt="Logo de la pagina">
                <li><a href="index_log.php">Inicio</a></li>
                <li><a href="https://sanmonor.github.io/Proyecto_Suplementos_Deportivos/" target="_blank"
                        rel="noopener noreferrer">Nuestro Portafolio</a></li>
                <li><a href="#">Términos y condiciones</a></li>
            </ul>
        </div>
        <div class="copyright">
            <p>&copy; 2023 Guía de suplementos deportivos. Todos los derechos reservados.</p>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript"></script>
    <script src="sweetAlert.js"></script>
</body>

</html>