<?php
    include_once "header/header_index.php";
?>

<head>
    <title>Crear cuenta | Guía de Suplementos Deportivos</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/styles_registro.css">
</head>

    <section class="formulario seccion-gris">
        <div class="container">
            <div class="row">
                <div class=" col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                    <form  action="includes/signup.inc.php" method="POST" id="formulario" autocomplete="off">
                        <h2>¡Comencemos!</h2>
                        <p>Regístrate para disfrutar de todo nuestro contenido</p><br>
                        <div class="form-group">
                            <label class="label" for="nombres"><strong>Nombres:</strong></label>
                            <input type="text" class="form-control" id="nombres" name="nombres"
                                placeholder="Ingresa tus nombres" required>
                        </div>
                        <div class="form-group">
                            <label class="label" for="apellidos"><strong>Apellidos:</strong></label>
                            <input type="text" class="form-control" id="apellidos" name="apellidos"
                                placeholder="Ingresa tus apellidos" required>
                        </div>

                        <div class="form-group">
                            <label class="label" for="telefono"><strong>Teléfono:</strong></label>
                            <input type="text" class="form-control" id="telefono" name="telefono"
                                placeholder="Ingresa un teléfono" pattern="[0-9]{10}" required>
                        </div>

                        <div class="form-group">
                            <label class="label" for="correo"><strong>Correo:</strong></label>
                            <input type="email" class="form-control" id="correo" name="correo"
                                placeholder="Ingresa un correo" required>
                        </div>

                        <div class="form-group">
                            <label class="label" for="contrasena"><strong>Contraseña:</strong></label>
                            <input type="password" class="form-control" id="contrasena" name="contrasena"
                                placeholder="Confirma la contraseña" minlength="8" required>
                        </div>

                        <div class="form-group">
                            <label class="label" for="confirmar_contrasena"><strong>Confirmar contraseña:</strong></label>
                            <input type="password" class="form-control" id="confirmar_contrasena" name="confirmar_contrasena"
                            placeholder="Ingresa una contraseña" minlength="8" required>
                        </div>
                        
                        <div class="form-group form_action--button">
                            <input class="btn btn-primary" type="submit" value="Registrar" id="btn_registrar"
                            name="btn_registrar">
                        </div>
                    </form>
                    <?php
                        if (!isset($_GET['error'])) {
                            
                        }
                        else {
                            $errorCheck = $_GET['error'];
                            
                            if ($errorCheck == "emptyinput") {
                                echo "<p class='error'>¡Ups! Parece que te has olvidado de rellenar algunos campos.</p>";
                            }
                            elseif ($errorCheck == "nameorlastname") {
                                echo "<p class='error'>¡Vaya! Parece que has utilizado algunos caracteres no válidos.</p>";
                            }
                            elseif ($errorCheck == "phone") {
                                echo "<p class='error'>¡Oh no! Parece que has ingresado un número de teléfono no válido.</p>";
                            }
                            elseif ($errorCheck == "email") {
                                echo "<p class='error'>¡Oh no! Parece que has ingresado un correo electrónico no válido.</p>";
                            }
                            elseif ($errorCheck == "passwordlenght") {
                                echo "<p class='error'>¡Oops! Parece que la longitud de tu contraseña no cumple con el requisito mínimo de 8 caracteres.</p>";
                            }
                            elseif ($errorCheck == "passwordmatch") {
                                echo "<p class='error'>¡Oh no! Parece que las contraseñas no coinciden.</p>";
                            }
                            elseif ($errorCheck == "emailtaken") {
                                echo "<p class='error'>¡Ups! Parece que ese correo electrónico ya está registrado en nuestro sistema.</p>";
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>

    <!-- Sección pie de pagina-->
    <footer>
        <div class="useful-links">
            <ul>
                <img src="img/logo_blanco.png" class="mb-3" width="100" alt="Logo de la pagina">
                <li><a href="index.php">Inicio</a></li>
                <li><a href="https://sanmonor.github.io/Proyecto_Suplementos_Deportivos/" target="_blank" rel="noopener noreferrer">Nuestro Portafolio</a></li>
                <li><a href="#">Términos y condiciones</a></li>
            </ul>
        </div>
        <div class="copyright">
            <p>&copy; 2023 Guía de suplementos deportivos. Todos los derechos reservados.</p>
        </div>
    </footer>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>
</body>

</html>